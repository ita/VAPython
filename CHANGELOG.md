# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to a versioning scheme similar to Matlab, with versions based on the release year.

## [Unreleased]

## [2024a] - 2024-09-26

### Added

- Add `CHANGELOG.md`, `CONTRIBURING.md`, and `CODE_OF_CONDUCT` files (!6)
- Add CI configuration (!4)
- Unit testing for most used functions in binding (!4)
- Documentation full for binding (!5)
- Add NatNetTracking support (!8) -> Feature parity with VAMatlab
- Add the option to add default values to the generated Python class (!10)
- Add event handling (!11)
- New examples (!7)

### Changed

- Complete rework of binding using `betterproto` for a pure Python implementation (!4)
- Set the minimum Python version to 3.9 (!9)
- CMakeLists.txt adapted to new, pure Python binding (!12)
- Improved README.md (!15)

## [2023b] - 2023-11-16

### Changed

- Improved README.md

## [2023a] - 2023-06-21

### Added

- Better wheel generation with multiple Python versions supported

### Fixed

- many minor bugs in binding

## [2022a] - 2022-06-15

### Changed

- Modernize build system

## Previous versions

- No changelog available