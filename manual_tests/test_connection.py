from vapython import VA


def callback(data):
    print(data)


va = VA()

print("Connecting to VA...")

va.connect()

print("Check if the server lists a connected client by pressing 'c' in the server window.")

input("Press Enter to continue with creating a sound receiver...")

va.create_sound_receiver("receiver")

print("Did the running server print the message 'Created sound receiver 'receiver' and assigned ID 1'?")

input("Press Enter to continue with attaching an event handler, expect event output.\n to stop press Enter again...")

va.attach_event_handler(callback)

input("Press Enter to continue with detaching an event handler...")

va.detach_event_handler(callback)

print("Did the printing stop?")

input("Press Enter to continue with creating a sound source...")

va.create_sound_source("source")

print("Did the running server print the message 'Created sound source 'source' and assigned ID 1'?")

input("Press Enter to continue with disconnecting from the server...")

va.disconnect()

print("Check if the server does not list a connected client by pressing 'c' in the server window.")

input("Press Enter to continue with reconnecting to the server...")

va.connect()

print("Check if the server lists a connected client by pressing 'c' in the server window.")

input("Press Enter to continue and disconnect again...")

va.disconnect()

print("Check if the server does not list a connected client by pressing 'c' in the server window.")

input("Press Enter to finish...")

print("Finished.")
