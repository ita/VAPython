import ast
import re
import subprocess
import sys
from importlib import resources
from pathlib import Path
from urllib.request import urlretrieve

import black
import isort
import jinja2

parent_path = Path(__file__).parent


if sys.version_info >= (3, 11, 0):
    import tomllib as toml
else:
    import toml


def download_proto_files(branch: str, base_path: Path) -> dict[str, Path]:
    """Download the proto files from the VANet repository and save them to the given base_path.

    Args:
        branch: The branch to download the files from.
        base_path: The base path to save the files to.
    """
    if not base_path.exists():
        base_path.mkdir(parents=True)

    branch_clean = re.sub(r'[<>:"/\\|?*]', "_", branch)
    download_base_path = base_path / branch_clean
    if not download_base_path.exists():
        download_base_path.mkdir(parents=True)

    proto_files = ["va_event", "va_exception", "va_struct", "va_messages", "va"]
    proto_data_base = {}
    for proto_file in proto_files:
        proto_file_url = f"https://git.rwth-aachen.de/ita/VANet/-/raw/{branch}/protos/{proto_file}.proto"
        file_path = download_base_path / f"{proto_file}.proto"

        urlretrieve(proto_file_url, file_path)  # noqa: S310

        proto_data_base[file_path.name] = file_path

    return proto_data_base


def generate_unified_proto_file(out_path: Path, proto_data_base: dict[str, Path]):
    """Generate a unified proto file from the given proto files.

    This function will merge the given proto files into a single proto file.
    This is necessary as the python betterproto library is easier to work with with a single file.

    Args:
        out_path: The path to save the unified proto file to.
        proto_data_base: The proto files to merge into the unified proto file.
    """
    output_file = out_path / "VA.proto"
    with open(output_file, "w") as out_file:
        out_file.write(
            r"""syntax = "proto3";
package vanet;

import "google/protobuf/empty.proto";
import "google/protobuf/wrappers.proto";
"""
        )
        for file in proto_data_base.values():
            with open(file) as in_file:
                for line in in_file.readlines():
                    if not re.findall("^(?:syntax)|(?:import)|(?:package)", line):
                        out_file.write(line)
            out_file.write("\n")

    return output_file


def generate_python_files(out_path: Path, proto_file_path: Path, proto_include_path: Path):
    """Generate the python files from the given proto file.

    This function uses the grpc_tools library and the python betterproto library to generate the python files.

    Args:
        out_path: The path to save the generated python files to.
        proto_file_path: The path to the proto file to generate the python files from.
        proto_include_path: The path to the proto include files.
    """

    def _get_resource_file_name(package_or_requirement: str, resource_name: str) -> str:
        """Obtain the filename for a resource on the file system."""
        file_name = None
        file_name = (resources.files(package_or_requirement) / resource_name).resolve()
        return str(file_name)

    if not out_path.exists():
        out_path.mkdir(parents=True)

    proto_include = _get_resource_file_name("grpc_tools", "_proto")

    args = f"--python_betterproto_out={out_path} -I{proto_include} -I{proto_include_path} {proto_file_path}"
    command = f"python -m grpc_tools.protoc {args}"
    result = subprocess.call(command, shell=True)  # noqa: S602

    if result != 0:
        error_message = f"protoc exited with return code {result}"
        raise RuntimeError(error_message)

    return out_path / "vanet" / "__init__.py"


def fix_generated_file(file: Path):
    """Fix the generated python file.

    This function fixes some minor issues that stem from the generation process and the way the proto files are written.

    To be specific:
        - since the proto files have both the package and the service name as VA, the generated python files have name collisions.
        - to fix this, the package nane is changed to vanet in `generate_unified_proto_file`
        - however, to work with the grpc library, the message route has to be changed from /vanet.VA/ to /VA.VA/

    Args:
        file: The file to fix.
    """
    # fix empty class, see: https://github.com/danielgtaylor/python-betterproto/issues/9
    with open(file) as f:
        lines = f.readlines()
    with open(file, "w") as f:
        f.write("# mypy: ignore-errors\n")
        for line in lines:
            fixed_line = line.replace("/vanet.VA/", "/VA.VA/")
            f.write(fixed_line)


def get_documentation():
    """Get the documentation for the methods in the VAInterface class.

    This method reads the documentation from the documentation.toml file and assembles the final docstrings.
    The docstrings are formatted following the google docstring style.
    The results are stored in a dictionary with the method names as keys and the docstrings as values, which is then returned.

    Returns:
        A dictionary with the method names as keys and the docstrings as values.
    """
    documentation_file = parent_path / "documentation.toml"
    if sys.version_info >= (3, 11, 0):
        with open(documentation_file, "rb") as f:
            data = toml.load(f)
    else:
        with open(documentation_file) as f:
            data = toml.load(f)

    final_doc = {}
    for key, value in data["VA"].items():
        docstring = ""

        if value.get("brief"):
            docstring += value["brief"] + "\n\n"

        if value.get("detail"):
            docstring += value["detail"] + "\n\n"

        if value.get("args"):
            docstring += "Args:\n"
            if isinstance(value["args"], str):
                docstring += f"    {value['args']}\n"
            else:
                for arg in value["args"]:
                    docstring += f"    {arg}\n"
            docstring += "\n"

        if value.get("returns"):
            docstring += "Returns:\n"
            if isinstance(value["returns"], str):
                docstring += f"    {value['returns']}\n"
            else:
                for ret in value["returns"]:
                    docstring += f"    {ret}\n"
            docstring += "\n"

        if value.get("raises"):
            docstring += "Raises:\n"
            if isinstance(value["raises"], str):
                docstring += f"    {value['raises']}\n"
            else:
                for exc in value["raises"]:
                    docstring += f"    {exc}\n"
            docstring += "\n"

        if value.get("post_doc"):
            docstring += value["post_doc"]

        if docstring[-2:] == "\n\n":
            docstring = docstring[:-1]

        final_doc[key] = docstring

    return final_doc


def get_default_values():
    """Get the default values for the methods in the VAInterface class.

    Returns:
        A dictionary with the method names as keys and the parameters with its default values as values.
    """
    default_values_file = parent_path / "default_values.toml"
    if sys.version_info >= (3, 11, 0):
        with open(default_values_file, "rb") as f:
            data = toml.load(f)
    else:
        with open(default_values_file) as f:
            data = toml.load(f)

    return data["VA"]


def parse_python_file(file_path: Path):
    """Parse the python file and extract the methods from the VAStub class.

    This function is where most of the magic happens. It reads the generated python file and extracts the methods from the VAStub class.
    The resulting data can then be used to render the template.

    The steps are as follows:
        - get the documentation and default values for the methods
        - read the generated grpc file
        - find the VAStub class and iterate over its methods
            - extract the method name
            - given the method name, extract the argument type and get the message type
            - transform the message type to pure python type or custom VA types
            - handle special cases:
                - bool parameters
                - default values
                - name fixes
                - private methods
            - get the return type of the method
        - build the output list with the method data

    Args:
        file_path: The path to the generated python file.

    Returns:
        A list of dictionaries with the method data.
    """
    documentation = get_documentation()
    default_values = get_default_values()

    with open(file_path) as file:
        all_code = ast.parse(file.read())

    name_fix = {
        "get_geometry_mesh_i_ds": "get_geometry_mesh_ids",
        "get_sound_portal_i_ds": "get_sound_portal_ids",
        "get_scene_i_ds": "get_scene_ids",
        "get_sound_receiver_i_ds": "get_sound_receiver_ids",
        "get_sound_source_i_ds": "get_sound_source_ids",
    }

    private_methods = [
        "get_state",
        "attach_event_handler",
    ]

    output_data = []

    for node in ast.walk(all_code):
        if isinstance(node, ast.ClassDef) and node.name == "VaStub":
            for n in node.body:
                if isinstance(n, (ast.AsyncFunctionDef, ast.FunctionDef)):
                    # Handle method arguments
                    message = [
                        {"name": arg.arg, "type": arg.annotation.value} for arg in n.args.args if arg.arg != "self"
                    ]

                    if message.__len__() > 1:
                        msg = f"Method '{n.name}' had more than one argument, which is not supported."
                        raise RuntimeError(msg)

                    message_type = message[0]["type"]
                    message_def = find_classdef(all_code, message_type)

                    members = []
                    if message_def is None:
                        match = re.search(r"protobuf\.(.*)Value", message_type)
                        if "protobuf.Empty" in message_type:
                            pass
                        # using this regex "protobuf\.(.*)Value" check if a group is found in message_type and use that
                        elif match:
                            value_type = match.group(1)
                            if value_type == "Bool":
                                members = [{"name": "value", "type": "bool"}]
                            elif value_type in {"Int32", "Int64"}:
                                members = [{"name": "value", "type": "int"}]
                            elif value_type in {"Float", "Double"}:
                                members = [{"name": "value", "type": "float"}]
                            elif value_type == "String":
                                members = [{"name": "value", "type": "str"}]
                            members[-1]["name_org"] = "value"
                        else:
                            msg = f"Message type {message_type} not found"
                            raise RuntimeError(msg)
                    elif message_type == "Struct":
                        members = [{"name": "data", "type": "VAStruct", "name_org": "fields"}]
                    else:
                        members = get_classdef_assigns(message_def)

                    # TODO: handle protobuf types
                    # TODO: handle struct, convert dict to struct ... and the other way around?!

                    kw_members = []
                    for member in members:
                        if member["type"] == "bool":
                            members.remove(member)
                            kw_members.append(member)

                    # Handle method return type
                    return_type = None
                    wrapped_return_type = False
                    type_overwrites = {
                        "Struct": "VAStruct",
                        "Vector3": "VAVector",
                        "Quaternion": "VAQuaternion",
                    }
                    if n.returns and isinstance(n.returns, ast.Constant):
                        match = re.search(r"protobuf\.(.*)Value", n.returns.value)
                        if "protobuf.Empty" in n.returns.value:
                            pass
                        elif n.returns.value in type_overwrites:
                            return_type = type_overwrites[n.returns.value]
                        elif match:
                            value_type = match.group(1)
                            wrapped_return_type = True
                            if value_type == "Bool":
                                return_type = "bool"
                            elif value_type in {"Int32", "Int64"}:
                                return_type = "int"
                            elif value_type in {"Float", "Double"}:
                                return_type = "float"
                            elif value_type == "String":
                                return_type = "str"
                        else:
                            # TODO: handle this and return tuples from the methods
                            return_type = "vanet." + n.returns.value

                    # Handle special cases
                    fixed_name = name_fix[n.name] if n.name in name_fix else n.name

                    if fixed_name in private_methods:
                        fixed_name = f"_{fixed_name}"

                    if fixed_name in default_values:
                        for member in members:
                            if member["name"] in default_values[fixed_name]:
                                member["default"] = default_values[fixed_name][member["name"]]

                        for member in kw_members:
                            if member["name"] in default_values[fixed_name]:
                                member["default"] = default_values[fixed_name][member["name"]]

                    members = sorted(members, key=lambda x: "default" in x)
                    kw_members = sorted(kw_members, key=lambda x: "default" in x)

                    if fixed_name == "_attach_event_handler":
                        continue

                    # Build output data
                    output_data.append(
                        {
                            "name": fixed_name,
                            "org_name": n.name,
                            "args": members,
                            "kwargs": kw_members,
                            "message_type": message_type,
                            "returns": return_type,
                            "wrapped_return_type": wrapped_return_type,
                            "docstring": (documentation[fixed_name] if fixed_name in documentation else ""),
                        }
                    )

    return output_data


def find_classdef(node: ast, class_name):
    """Find a class definition in the given AST node.

    Helper function for `parse_python_file`.

    Args:
        node: The AST node to search in.
        class_name: The name of the class to find.

    Returns:
        The class definition node if found, otherwise None
    """
    for n in ast.walk(node):
        if isinstance(n, ast.ClassDef) and n.name == class_name:
            return n
    return None


def get_classdef_assigns(classdef: ast.ClassDef):
    """Get the assignments of a class definition.

    Helper function for `parse_python_file`.

    Args:
        classdef: The class definition to get the assignments from.

    Returns:
        A list of dictionaries with the name, type, and original name of the assignments.
    """
    assigns = []
    for n in classdef.body:
        if isinstance(n, ast.AnnAssign):
            type_name = ""

            if isinstance(n.annotation, ast.Name):
                type_name = n.annotation.id
            else:
                if sys.version_info >= (3, 9, 0) and isinstance(n.annotation, ast.Constant):
                    type_name = n.annotation.value
                elif sys.version_info < (3, 9, 0) and isinstance(n.annotation, ast.Str):
                    type_name = n.annotation.s

                type_overwrites = {
                    "Struct": "VAStruct",
                    "Vector3": "Union[VAVector, List[float], Tuple[float, float, float]]",
                    "Quaternion": "Union[VAQuaternion, List[float], Tuple[float, float, float,float]]",
                }
                type_name = "vanet." + type_name if type_name not in type_overwrites else type_overwrites[type_name]

            overwrites = {
                "id": "id_",
                "type": "type_",
            }
            assigns.append(
                {
                    "name": (n.target.id if n.target.id not in overwrites else overwrites[n.target.id]),
                    "type": type_name,
                    "name_org": n.target.id,
                }
            )
    return assigns


def render_template(data, out_file: Path):
    """Render the template with the given data and save it to the given file.

    This uses the jinja2 library to render the template.

    Args:
        data: The data to render the template with.
        out_file: The file to save the rendered template to.
    """
    if not isinstance(out_file, Path):
        out_file = Path(out_file)

    template_loader = jinja2.FileSystemLoader(searchpath=parent_path / "templates")

    template_env = jinja2.Environment(
        loader=template_loader,
        trim_blocks=True,
        lstrip_blocks=True,
        autoescape=jinja2.select_autoescape(
            disabled_extensions=("py.j2",),
            default_for_string=True,
            default=False,
        ),
    )
    template = template_env.get_template("wrapper.py.j2")

    rendered_template = template.render(data)

    with open(out_file, "w") as f:
        f.write(rendered_template)

    black.format_file_in_place(out_file, write_back=black.WriteBack.YES, fast=True, mode=black.FileMode())

    isort.file(out_file)


def build_vapython(base_path: Path, output_wrapper_file: Path, branch: str = "master"):
    """Build the VAInterface python file.

    This function is the main entry point for the script. It orchestrates the whole process of generating the VAInterface python file.

    Args:
        base_path: The base path to save the generated files to.
        output_wrapper_file: The file to save the generated VAInterface python file to.
        branch: The branch to download the proto files from.
    """
    download_path = base_path / "download"
    stub_path = base_path / "stubs"

    proto_data_base = download_proto_files(branch, download_path)

    output_file = generate_unified_proto_file(download_path, proto_data_base)

    output_python_file = generate_python_files(stub_path, output_file, output_file.parent)

    fix_generated_file(output_python_file)

    output = parse_python_file(output_python_file)

    render_template({"methods": output}, output_wrapper_file)


if __name__ == "__main__":
    base_path = Path("generation")
    branch = "feature/grpc-improvement"

    build_vapython(base_path, parent_path / "vapy.py", branch)
