import shutil
import sys
from pathlib import Path

from hatchling.builders.hooks.plugin.interface import BuildHookInterface

parent_path = Path(__file__).parent
sys.path.append(str(parent_path))
from build_vapython import build_vapython  # noqa: E402


class CustomBuildHook(BuildHookInterface):
    def initialize(self, version, build_data):
        root = Path(self.root)

        build_dir = root / "build" / f"vapython-{version}"
        src_dir = root / "src" / "vapython"

        if not build_dir.exists():
            build_dir.mkdir(parents=True)

        branch = "master"
        installed_git = shutil.which("git")
        if installed_git:
            from subprocess import check_output

            branch = (
                check_output([installed_git, "rev-parse", "--abbrev-ref", "HEAD"], cwd=src_dir).decode("utf-8").strip()
            )

            if branch not in ["master", "develop"]:
                branch = "develop"

        build_vapython(build_dir, build_dir / "va_python.py", branch)

        vanet_dir = src_dir / "vanet"

        if not vanet_dir.exists():
            vanet_dir.mkdir(parents=True)

        shutil.copy(build_dir / "stubs" / "vanet" / "__init__.py", vanet_dir / "_vanet_grpc.py")
        shutil.copy(build_dir / "va_python.py", vanet_dir / "_va_interface.py")

        build_data["artifacts"].append(str((vanet_dir / "_vanet_grpc.py").relative_to(self.root)))
        build_data["artifacts"].append(str((vanet_dir / "_va_interface.py").relative_to(self.root)))
