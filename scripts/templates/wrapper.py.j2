import asyncio
import concurrent
from threading import Thread
from typing import Callable, List, Optional, Tuple, Union

from grpclib.client import Channel
from grpclib.const import Cardinality

import vapython.vanet._helper as vanet_helper
import vapython.vanet._vanet_grpc as vanet
from vapython._types import VAStruct, VAVector, VAQuaternion

class VAInterface:
    """A class for interfacing with the VA server.

    This class is used to connect to the VA server and call its interface methods.
    Note that this is generated code and should not be modified.

    The overwritten class [VA][vapython.va.VA] should be used instead of this class.
    """

    def __init__(self) -> None:
        """Initializes the VAInterface object"""
        self.__service: Optional[vanet.VaStub] = None
        self._channel: Optional[Channel] = None
        self._event_callbacks: List[Callable[[vanet.Event], None]] = []
        self._event_thread: Optional[Thread] = None
        self._connected: bool = False
        self._event_thread_future: Optional[concurrent.futures.Future] = None
        self.__loop: Optional[asyncio.AbstractEventLoop] = None

    def __del__(self):
        """Destructor for the VAInterface object.

        Closes the channel if it is open.
        """

        self.disconnect()

    def connect(self, host: str = "localhost", port: int = 12340, *, add_event_handling: bool = True) -> None:
        """Connects to the VA server.

        Args:
            host: The hostname of the VA server.
            port: The port number of the VA server.
            add_event_handling: If True, VAPython will receive events from the VAServer.
                If False, no events will be received and the server will not be able to determine that a client is connected.
                However, not sending events can reduce network traffic.
        """

        self._channel = Channel(host=host, port=port)

        self.__loop = self._channel._loop

        self.__service = vanet.VaStub(self._channel)

        def background_event_tread(loop: asyncio.AbstractEventLoop) -> None:
            asyncio.set_event_loop(loop)
            loop.run_forever()

        self._event_thread = Thread(target=background_event_tread, args=(self._loop,) , daemon=True)
        self._event_thread.start()

        try:
            self._get_state()
        except ConnectionError as e:
            print(f"Could not connect to VA server. Error: {e}")
            print("Please make sure the VA server is running and accessible.")
            self.disconnect()
            exit(1)

        self._connected = True

        if add_event_handling:
            # Event loop in separate thread, based on: https://gist.github.com/dmfigol/3e7d5b84a16d076df02baa9f53271058
            async def handle_events():
                async with self._service.channel.request(
                    "/VA.VA/AttachEventHandler",
                    Cardinality.UNARY_STREAM,
                    vanet.betterproto_lib_google_protobuf.Empty,
                    vanet.Event,
                ) as stream:
                    await stream.send_message(
                        vanet.betterproto_lib_google_protobuf.Empty()
                    )

                    while self._connected:
                        event = await stream.recv_message()
                        if event is None:
                            await stream.end()
                            break
                        else:
                            for event_callback in self._event_callbacks:
                                event_callback(event)
                            await asyncio.sleep(0.1)

                    await stream.cancel()

            self._event_thread_future = asyncio.run_coroutine_threadsafe(handle_events(), self._loop)

    def disconnect(self):
        """Disconnects from the VA server."""
        self._connected = False
        if self._event_thread_future:
            self._event_thread_future.result()

        if self.__loop and self._loop.is_running():
            self.__loop.call_soon_threadsafe(self.__loop.stop)

        if self._event_thread:
            self._event_thread.join()
            self._event_thread = None

        if self._channel:
            self._channel.close()

    def get_server_address(self) -> Optional[str]:
        """Returns the address of the VA server.

        Returns:
            The address of the VA server. If not connected to a server, returns `None`.
        """

        if self._channel:
            host = self._channel._host
            port = self._channel._port
            return f"{host}:{port}"
        return None

    {% for method in methods %}
    def {{ method.name }}(self,
    {% for arg in method.args %}
        {{ arg.name}}: {{ arg.type }} {% if arg.default %} = {{ arg.default }}{% endif %},
    {% endfor %}
    {% if method.kwargs %}
        *,
    {% for kwarg in method.kwargs %}
        {{ kwarg.name }}: {{ kwarg.type }} {% if kwarg.default %} = {{ kwarg.default }}{% endif %},
    {% endfor %}
    {% endif %}
    ){% if method.returns %} -> {{ method.returns }}{% endif %}:
        {% if method.docstring %}
        """{{ method.docstring -}}
        """

        {% endif %}
        return_value = asyncio.run_coroutine_threadsafe(
            self._service.{{ method.org_name }}(
                vanet.{{ method.message_type }}(
                    {% for arg in method.args %}
                    {% if arg.type == "VAStruct" %}
                    {{ arg.name_org }}=vanet_helper.convert_struct_to_vanet({{ arg.name }}),
                    {% elif "VAVector" in arg.type %}
                    {{ arg.name_org }}=vanet_helper.convert_vector_to_vanet({{ arg.name }}),
                    {% elif "VAQuaternion" in arg.type %}
                    {{ arg.name_org }}=vanet_helper.convert_quaternion_to_vanet({{ arg.name }}),
                    {% else %}
                    {{ arg.name_org }}={{ arg.name }},
                    {% endif %}
                    {% endfor %}
                    {% for arg in method.kwargs %}
                    {{ arg.name_org }}={{ arg.name }},
                    {% endfor %}
                )
            ),
            self._loop
        ).result()
        {% if not method.wrapped_return_type %}
        {% if method.returns == "VAStruct" %}
        return vanet_helper.convert_struct_from_vanet(return_value)
        {% elif method.returns == "VAVector" %}
        return vanet_helper.convert_vector_from_vanet(return_value)
        {% elif method.returns == "VAQuaternion" %}
        return vanet_helper.convert_quaternion_from_vanet(return_value)
        {% else %}
        return return_value
        {% endif %}
        {% else %}
        return return_value.value
        {% endif %}

    {% endfor %}

    @property
    def _service(self) -> vanet.VaStub:
        """The service object for the VA server.

        Returns:
            The service object for the VA server.
        """
        if not self.__service:
            msg = "Not connected to a server"
            raise ValueError(msg)

        return self.__service

    @property
    def _loop(self) -> asyncio.AbstractEventLoop:
        """The event loop.

        Returns:
            The event loop.
        """
        if not self.__loop:
            msg = "Not connected to a server"
            raise ValueError(msg)

        return self.__loop

    def attach_event_handler(self, callback: Callable[[vanet.Event], None]) -> None:
        """Attaches a callback function to the event handler (for advaned users).

        Generally, the VAServer sends event messages to connected interfaces whenever something in the VACore changes,
        e.g. a sound source is created or changes its position. An interface can connect a callback function to react
        to those events.

        See also [`detach_event_handler()`][vapython.va.VA.detach_event_handler].

        Examples:
        ```python
        from vapython import VA
        def callback(event_data)
            print(event_data)
        
        # This will print the output of every event sent by the VAServer
        # Careful: This might lead to spamming your terminal output.
        va.attach_event_handler(callback)
        ```

        Args:
            callback: The callback function to attach.
        """
        self._event_callbacks.append(callback)

    def detach_event_handler(self, callback: Callable[[vanet.Event], None]) -> None:
        """Detaches a previously attached callback function from the event handler (for advaned users).

        See also [`attach_event_handler()`][vapython.va.VA.attach_event_handler].

        Args:
            callback: The callback function to detach.
        """
        self._event_callbacks.remove(callback)