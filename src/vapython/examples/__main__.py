import os
import sys
from pathlib import Path


def main():
    """
    Main function to list and open example scripts.
    This function performs the following steps:
    1. Lists all Python files in the current directory that contain "example" in their name.
    2. If no command-line arguments are provided, it prints the list of example files and prompts the user to select one by index.
    3. If a command-line argument is provided, it uses that as the index of the example file to open.
    4. Validates the selected index and either exits, prints an error message, or opens the file.
    Returns:
        None
    """

    current_path = Path(__file__).parent

    example_files = list(current_path.glob("*example*.py"))

    if len(sys.argv) == 1:
        for i, example_file in enumerate(example_files):
            print(f"{i}: {example_file.stem}")

        print(f"{len(example_files)}: Exit")

        example_index = int(input("Enter the index of the example you want to open: "))
    else:
        example_index = int(sys.argv[1])

    if example_index == len(example_files):
        return

    if example_index < 0 or example_index >= len(example_files):
        print("Invalid index")
        return

    example_file = example_files[example_index]
    os.startfile(example_file)  # noqa: S606


if __name__ == "__main__":
    main()
