"""VA offline simulation/auralization example

Requires VA to run with a virtual audio device that can be triggered by
the user. Also the rendering module(s) have to be set to record the output
to hard drive. To shutdown the server from Matlab, remote control modus
must be activated. Finally, the utilized buffer size and sampling rate
specified in the VACore.ini must match the parameters used here.

You can run the run_VAServer_recording.bat batch script to start a
VAServer using the required settings. This is located in VA's main folder.
"""

import numpy as np

from vapython import VA

buffer_size = 64
sampling_rate = 44100

# Connect and set up simple scene
va = VA()

va.connect("localhost")

L = va.create_sound_receiver("VA_Listener")
va.set_sound_receiver_position(L, [0, 1.7, 0])
H = va.create_directivity_from_file("$(DefaultHRIR)")
va.set_sound_receiver_directivity(L, H)

S = va.create_sound_source("VA_Source")
X = va.create_signal_source_buffer_from_file("$(DemoSound)")
va.set_signal_source_buffer_playback_action(X, "play")
va.set_signal_source_buffer_looping(X, looping=True)
va.set_sound_source_signal_source(S, X)

# Example for a synchronized scene update & audio processing simulation/auralization

time_step = buffer_size / sampling_rate  # here: depends on block size and sample rate
manual_clock = 0.0
va.set_core_clock(0)

spatial_step = 0.005
print(f"Resulting sound source speed: {spatial_step / time_step} m/s")

num_steps = 6000
print(f"Simulation result duration: {num_steps * time_step} s")

x = np.linspace(-100, 100, num=num_steps)  # motion from x = -100m to x = 100m

print("Hold on, running auralization")
for n in range(len(x)):
    # Modify scene as you please
    va.set_sound_source_position(S, [x[n], 1.7, -3])

    # Increment core clock
    manual_clock += time_step
    va.call_module("manualclock", {"time": manual_clock})

    # Process audio chain by incrementing one block
    va.call_module("virtualaudiodevice", {"trigger": True})

    print(f"\rProgress: {(n + 1) / num_steps:.2%}", end="")

print("\nSimulation completed.")

va.shutdown_server()
va.disconnect()
