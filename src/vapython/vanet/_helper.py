# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

"""Helper functions for converting between VAPython and Vanet gRPC types.

This is used as a compatibility layer between native Python and the gRPC world.
"""

from typing import Union

import betterproto

from vapython._types import VAQuaternion, VAStruct, VAVector
from vapython.vanet._vanet_grpc import Quaternion as VanetQuaternion
from vapython.vanet._vanet_grpc import SampleBuffer as VanetSampleBuffer
from vapython.vanet._vanet_grpc import Struct as VanetStruct
from vapython.vanet._vanet_grpc import Value as VanetStructValue
from vapython.vanet._vanet_grpc import Vector3 as VanetVector


def convert_struct_to_vanet(input_struct: VAStruct) -> VanetStruct:
    """Converts a VAStruct or dictionary to a VanetStruct.

    Args:
        input_struct: The input structure to be converted.

    Returns:
        The converted VanetStruct.

    Raises:
        TypeError: If the input_struct is not of type VAStruct or dict.
        ValueError: If an unknown type of value is encountered in the struct.
    """
    if not isinstance(input_struct, dict):
        msg = "input_struct must be of type VAStruct or dict"
        raise TypeError(msg)

    output_struct = VanetStruct()
    for key, value in input_struct.items():
        if isinstance(value, bool):
            output_struct.fields[key] = VanetStructValue(bool_value=value)
        elif isinstance(value, int):
            output_struct.fields[key] = VanetStructValue(integer_value=value)
        elif isinstance(value, float):
            output_struct.fields[key] = VanetStructValue(double_value=value)
        elif isinstance(value, str):
            output_struct.fields[key] = VanetStructValue(string_value=value)
        elif isinstance(value, dict):
            output_struct.fields[key] = VanetStructValue(struct_value=convert_struct_to_vanet(value))
        elif isinstance(value, bytes):
            output_struct.fields[key] = VanetStructValue(data_value=value)
        elif isinstance(value, list) and all(isinstance(x, float) for x in value):
            output_struct.fields[key] = VanetStructValue(buffer_value=VanetSampleBuffer(value))
        else:
            msg = "Unknown type of value in struct!"
            raise ValueError(msg)

    return output_struct


def convert_struct_from_vanet(input_struct: VanetStruct) -> VAStruct:
    """
    Converts a VanetStruct object to a VAStruct object.

    Args:
        input_struct: The input VanetStruct object to be converted.

    Returns:
        The converted VAStruct object.

    Raises:
        TypeError: If the input_struct is not of type VanetStruct.
        ValueError: If the value in the struct is not set or if the kind of value is unknown.
    """
    if not isinstance(input_struct, VanetStruct):
        msg = "input_struct must be of type VanetStruct"
        raise TypeError(msg)

    output_struct: VAStruct = {}
    for key, value in input_struct.fields.items():
        if betterproto.which_one_of(value, "kind")[0] == "bool_value":
            output_struct[key] = value.bool_value
        elif betterproto.which_one_of(value, "kind")[0] == "integer_value":
            output_struct[key] = value.integer_value
        elif betterproto.which_one_of(value, "kind")[0] == "double_value":
            output_struct[key] = value.double_value
        elif betterproto.which_one_of(value, "kind")[0] == "string_value":
            output_struct[key] = value.string_value
        elif betterproto.which_one_of(value, "kind")[0] == "struct_value":
            output_struct[key] = convert_struct_from_vanet(value.struct_value)
        elif betterproto.which_one_of(value, "kind")[0] == "data_value":
            output_struct[key] = value.data_value
        elif betterproto.which_one_of(value, "kind")[0] == "buffer_value":
            output_struct[key] = value.buffer_value.samples
        elif betterproto.which_one_of(value, "kind")[0] == "":
            msg = "Value in struct not set!"
            raise ValueError(msg)
        else:
            msg = "Unknown kind of value in struct!"
            raise ValueError(msg)

    return output_struct


def convert_vector_to_vanet(input_vector: Union[VAVector, list[float], tuple[float, float, float]]) -> VanetVector:
    """
    Converts a vector to a VanetVector object.

    Args:
        input_vector: The input vector to be converted.

    Returns:
        The converted VanetVector object.

    Raises:
        ValueError: If the input vector is not of type VAVector, list, or tuple,
            or if the length of the input vector is not 3.
    """
    if isinstance(input_vector, VAVector):
        return VanetVector(x=input_vector.x, y=input_vector.y, z=input_vector.z)

    if isinstance(input_vector, (list, tuple)):
        if len(input_vector) != 3:  # noqa: PLR2004
            msg = "Vector must be of length 3"
            raise ValueError(msg)
        return VanetVector(x=input_vector[0], y=input_vector[1], z=input_vector[2])

    msg = "Vector must be of type VAVector, list or tuple"
    raise ValueError(msg)


def convert_vector_from_vanet(input_vector: VanetVector) -> VAVector:
    """
    Converts a VanetVector object to a VAVector object.

    Args:
        input_vector: The VanetVector object to be converted.

    Returns:
        The converted VAVector object.
    """
    return VAVector(x=input_vector.x, y=input_vector.y, z=input_vector.z)


def convert_quaternion_to_vanet(
    input_quaternion: Union[VAQuaternion, list[float], tuple[float, float, float, float]],
) -> VanetQuaternion:
    """
    Converts a quaternion to a VanetQuaternion object.

    Args:
        input_quaternion: The input quaternion to be converted.

    Returns:
        The converted VanetQuaternion object.

    Raises:
        ValueError: If the input quaternion is not of type VAQuaternion, list, or tuple, or if its length is not 4.
    """
    if isinstance(input_quaternion, VAQuaternion):
        return VanetQuaternion(x=input_quaternion.x, y=input_quaternion.y, z=input_quaternion.z, w=input_quaternion.w)

    if isinstance(input_quaternion, (list, tuple)):
        if len(input_quaternion) != 4:  # noqa: PLR2004
            msg = "Quaternion must be of length 4"
            raise ValueError(msg)
        return VanetQuaternion(
            x=input_quaternion[0], y=input_quaternion[1], z=input_quaternion[2], w=input_quaternion[3]
        )

    msg = "Quaternion must be of type VAQuaternion, list or tuple"
    raise ValueError(msg)


def convert_quaternion_from_vanet(input_quaternion: VanetQuaternion) -> VAQuaternion:
    """
    Converts a VanetQuaternion object to a VAQuaternion object.

    Args:
        input_quaternion (VanetQuaternion): The input VanetQuaternion object to be converted.

    Returns:
        VAQuaternion: The converted VAQuaternion object.
    """
    return VAQuaternion(x=input_quaternion.x, y=input_quaternion.y, z=input_quaternion.z, w=input_quaternion.w)
