# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

from __future__ import annotations

import time
import warnings
from pathlib import Path
from typing import Any, Optional, Union

from scipy.spatial.transform import Rotation  # type: ignore

import vapython._helper as helper
import vapython._types as va_types
import vapython.vanet._vanet_grpc as va_grpc
from vapython import tracking
from vapython.vanet import VAInterface


class VA(VAInterface):
    """
    Python interface to connect to and control a VAServer instance.

    Examples:
        >>> va = VA()
        >>> # This will connect to a VAServer running on the same computer (localhost with port 12340)
        >>> # NOTE: Generally, a VAServer can run on another computer using the respective IP address
        >>> va.connect()
        >>> va.create_sound_source("source1")

    Todo:
        - get_server is called get_server_state in matlab. Should we rename it?
        - what should get_server return? A enum?
        - `attach_event_handler` raise warning or smth.
        - how is a non-connected call handled?
        - what about logging? in comb with event handling? integrate the VAServer output?
    """

    @staticmethod
    def start_server(
        config_ini_file: Union[str, Path, None] = None,
        va_server_path: Union[str, Path, None] = None,
        extra_args: Union[list[str], None] = None,
        *,
        dedicated_window: bool = True,
    ) -> None:
        """
        Starts a VAServer on this computer (localhost:12340) using remote control mode.
        Use shutdown_server() method to stop the server again.

        Args:
            config_ini_file: Path to the VAServer configuration file.
            va_server_path: Path to the VAServer executable.
            extra_args: Extra arguments to pass to the VAServer executable.
        """
        import subprocess

        if va_server_path is None:
            va_server_path = helper.find_server_executable()

        if isinstance(va_server_path, str):
            va_server_path = Path(va_server_path)

        if va_server_path is None or not Path(va_server_path).is_file():
            msg = "Could not find valid VAServer executable."
            raise FileNotFoundError(msg)

        if config_ini_file is None:
            va_base_dir = va_server_path.parent.parent
            config_ini_file = va_base_dir / "conf" / "VACore.ini"

        if isinstance(config_ini_file, str):
            config_ini_file = Path(config_ini_file)

        if not config_ini_file.is_file():
            msg = "Could not find valid VACore.ini file."
            raise FileNotFoundError(msg)

        if extra_args is None:
            extra_args = []

        server_call: list[Union[str, Path]] = [
            va_server_path,
            "--config",
            config_ini_file,
            "--server-address",
            "localhost:12340",
            "--remote",
            *extra_args,
        ]

        popen_kwargs: dict[str, Any] = {}
        if dedicated_window:
            popen_kwargs["creationflags"] = subprocess.CREATE_NEW_CONSOLE

        # TODO: if no dedicated window is used, reroute the server output to logging ?!

        subprocess.Popen(
            server_call,
            **popen_kwargs,
        )

    def __init__(self) -> None:
        """Constructor for the VA class."""
        super().__init__()
        self._timer_interval: Optional[float] = None
        self._timer_last_call: Optional[int] = None
        self._tracker: Optional[tracking.Tracker] = None

        self._tracker_data: dict[
            int,
            Union[
                tracking.ReceiverTrackingData,
                tracking.ReceiverTorsoTrackingData,
                tracking.ReceiverRealWorldTrackingData,
                tracking.ReceiverRealWorldTorsoTrackingData,
                tracking.SourceTrackingData,
            ],
        ] = {}

    def get_global_auralization_mode(self, *, short_form: bool = True) -> str:  # type: ignore[override]
        """Get the [global auralization mode](https://www.virtualacoustics.org/VA/documentation/control/#global-auralization-mode).

        Args:
            short_form (optional): Whether to return the short form of the auralization mode. Defaults to True.

        Returns:
            A string representation of the auralization mode.

        Examples:
            >>> va.get_global_auralization_mode()
            "DS,ER"
        """
        return helper.convert_aura_mode_to_str(
            super().get_global_auralization_mode(),
            short_form=short_form,
        )

    def set_global_auralization_mode(self, mode: str) -> None:  # type: ignore[override]
        """Set the [global auralization mode](https://www.virtualacoustics.org/VA/documentation/control/#global-auralization-mode).

        Args:
            mode: The auralization mode to set.

        Examples:
            >>> va.set_global_auralization_mode("+DS,-ER")

            This will enable the Direct Sound and disable the Early Reflections.
        """
        super().set_global_auralization_mode(
            helper.parse_aura_mode_str(
                mode,
                super().get_global_auralization_mode(),
            )
        )

    def get_sound_receiver_auralization_mode(self, receiver_id: int, *, short_form: bool = True) -> str:  # type: ignore[override]
        """Get the auralization mode for a specific sound receiver.

        See also [get_global_auralization_mode()][vapython.va.VA.get_global_auralization_mode] for a quick example.

        Args:
            receiver_id: The ID of the sound receiver.
            short_form (optional): Whether to return the short form of the auralization mode. Defaults to True.

        Returns:
            A string representation of the auralization mode.
        """
        return helper.convert_aura_mode_to_str(
            super().get_sound_receiver_auralization_mode(receiver_id),
            short_form=short_form,
        )

    def set_sound_receiver_auralization_mode(self, receiver_id: int, mode: str) -> None:  # type: ignore[override]
        """Set the auralization mode for a specific sound receiver.

        See also [set_global_auralization_mode()][vapython.va.VA.set_global_auralization_mode] for a quick example.

        Args:
            receiver_id: The ID of the sound receiver.
            mode: The auralization mode to set.
        """
        super().set_sound_receiver_auralization_mode(
            receiver_id,
            helper.parse_aura_mode_str(
                mode,
                super().get_sound_receiver_auralization_mode(receiver_id),
            ),
        )

    def get_sound_source_auralization_mode(self, source_id: int, *, short_form: bool = True) -> str:  # type: ignore[override]
        """Get the auralization mode for a specific sound source.

        See also [get_global_auralization_mode()][vapython.va.VA.get_global_auralization_mode] for a quick example.

        Args:
            source_id: The ID of the sound source.
            short_form (optional): Whether to return the short form of the auralization mode. Defaults to True.

        Returns:
            A string representation of the auralization mode.
        """
        return helper.convert_aura_mode_to_str(
            super().get_sound_source_auralization_mode(source_id),
            short_form=short_form,
        )

    def set_sound_source_auralization_mode(self, source_id: int, mode: str) -> None:  # type: ignore[override]
        """Set the auralization mode for a specific sound source.

        See also [set_global_auralization_mode()][vapython.va.VA.set_global_auralization_mode] for a quick example.

        Args:
            source_id (int): The ID of the sound source.
            mode (str): The auralization mode to set.
        """
        super().set_sound_source_auralization_mode(
            source_id,
            helper.parse_aura_mode_str(
                mode,
                super().get_sound_source_auralization_mode(source_id),
            ),
        )

    def get_rendering_module_auralization_mode(self, renderer_id: str, *, short_form: bool = True) -> str:  # type: ignore[override]
        """Get the auralization mode for a specific rendering module.

        See also [get_global_auralization_mode()][vapython.va.VA.get_global_auralization_mode] for a quick example.

        Args:
            renderer_id: The ID of the sound source.
            short_form (optional): Whether to return the short form of the auralization mode. Defaults to True.

        Returns:
            A string representation of the auralization mode.
        """
        return helper.convert_aura_mode_to_str(
            super().get_rendering_module_auralization_mode(renderer_id),
            short_form=short_form,
        )

    def set_rendering_module_auralization_mode(self, renderer_id: str, mode: str) -> None:  # type: ignore[override]
        """Set the auralization mode for a specific rendering module.

        See also [set_global_auralization_mode()][vapython.va.VA.set_global_auralization_mode] for a quick example.

        Args:
            renderer_id (int): The ID of the sound source.
            mode (str): The auralization mode to set.
        """
        super().set_rendering_module_auralization_mode(
            renderer_id,
            helper.parse_aura_mode_str(
                mode,
                super().get_rendering_module_auralization_mode(renderer_id),
            ),
        )

    def get_signal_source_buffer_playback_state(self, signal_source_id: str) -> va_grpc.PlaybackStateState:  # type: ignore[override]
        """Get the playback state of a signal source buffer.

        Args:
            signal_source_id: The ID of the signal source to get the playback state for.

        Returns:
            The playback state of the signal source buffer.
        """

        state = super().get_signal_source_buffer_playback_state(signal_source_id).state
        return va_grpc.PlaybackStateState(state)

    def set_signal_source_buffer_playback_action(
        self,
        signal_source_id: str,
        playback_action: Union[str, va_grpc.PlaybackActionAction],  # type: ignore[override]
    ):  # type: ignore[override]
        """Set the playback action of a signal source buffer.

        Args:
            signal_source_id: The ID of the signal source to set the playback action for.
            playback_action: The playback action to set.

        Examples:
            >>> va.set_signal_source_buffer_playback_action("source1", "PLAY")
            >>> va.set_signal_source_buffer_playback_action(
            ...     "source1", va_grpc.PlaybackActionAction.PLAY
            ... )
        """

        if isinstance(playback_action, str):
            playback_action = va_grpc.PlaybackActionAction[playback_action.upper()]  # type: ignore[misc]

        super().set_signal_source_buffer_playback_action(
            signal_source_id,
            va_grpc.PlaybackAction(action=playback_action),  # type: ignore[arg-type]
        )

    def add_search_path(self, path: Union[str, Path]) -> bool:
        """Add a search path to the server.

        Args:
            path: The path to add.

        Returns:
            True if the path is valid, False otherwise.

        Examples:
            >>> str_path = "C:/path/to/folder"
            >>> va.add_search_path(str_path)
            True
            >>> path = Path("C:/path/to/folder")
            >>> va.add_search_path(path)
            True
        """

        arguments: va_types.VAStruct = {"addsearchpath": str(path)}

        result = super().call_module("VACore", arguments)
        return bool(result["pathvalid"])

    def create_signal_source_buffer_from_file(self, file_path: Union[str, Path], name: str = "") -> str:
        """Create a signal source buffer from a file.

        Note:
            The audiofile must be mono and its sampling rate must match that of the server.

        Args:
            file_path: The path to the file.
            name: The name of the signal source buffer.
        """

        parameters: va_types.VAStruct = {"filepath": str(file_path)}

        return super().create_signal_source_buffer_from_parameters(name=name, parameters=parameters)

    def create_directivity_from_file(self, file_path: Union[str, Path], name: str = "") -> int:
        """Create a source or receiver directivity from a file.

        Args:
            file_path: The path to the file.
            name: The name of the directivity.
        """

        parameters: va_types.VAStruct = {"filepath": str(file_path)}

        return super().create_directivity_from_parameters(name=name, parameters=parameters)

    def get_server_state(self) -> va_grpc.CoreState:
        """Get the state of the server.

        This indicates the state of the server, which can be one of the following:
        - `CREATED`: server started but not yet initialized
        - `READY`: successfully initialized and ready for use
        - `FAIL`: corrupted and can not be recovered

        Returns:
            The state of the server.
        """

        return super()._get_state()

    def remove_sound_source_signal_source(self, sound_source_id: int) -> None:
        """Remove the signal source from the sound source.

        Note: this will not delete the signal source, i.e. it can still be used by other sound sources.

        Args:
            sound_source_id: The ID of the sound source.
        """

        super().set_sound_source_signal_source(sound_source_id, "")

    def shutdown_server(self) -> None:
        """Shutdown the connected server"""
        arguments: va_types.VAStruct = {"shutdown": True}

        super().call_module("VACore", arguments)

    def get_sound_receiver_orientation_view_up(
        self, sound_receiver_id: int
    ) -> va_grpc.GetSoundReceiverOrientationVuReply:
        """Get the orientation of a sound receiver as a view-up vector pair.

        Alias for / same as [`get_sound_receiver_orientation_vu`][vapython.va.VA.get_sound_receiver_orientation_vu].

        Args:
            sound_receiver_id: The ID of the sound receiver to get the orientation of.

        Returns:
            The orientation of the sound receiver as a view-up vector pair.
        """
        return super().get_sound_receiver_orientation_vu(sound_receiver_id)

    def get_sound_receiver_real_world_head_position_orientation_view_up(
        self, sound_receiver_id: int
    ) -> va_grpc.GetSoundReceiverRealWorldPositionOrientationVuReply:
        """Get the real-world position and orientation of a sound receiver.

        Alias for / same as
        [`get_sound_receiver_real_world_position_orientation_vu`][vapython.va.VA.get_sound_receiver_real_world_position_orientation_vu].

        Note:
            Coordinates refer the to center of the head on the axis which goes through both ears.

        Args:
            sound_receiver_id: The ID of the sound receiver to get the real-world position and orientation of.

        Returns:
            The real-world position and orientation of the sound receiver.
        """
        return super().get_sound_receiver_real_world_position_orientation_vu(sound_receiver_id)

    def get_sound_source_orientation_view_up(self, sound_source_id: int) -> va_grpc.GetSoundSourceOrientationVuReply:
        """Get the orientation of a sound source as a view-up vector pair.

        Alias for / same as [`get_sound_source_orientation_vu`][vapython.va.VA.get_sound_source_orientation_vu].

        Args:
            sound_source_id: The ID of the sound source to get the orientation of.

        Returns:
            The orientation of the sound source as a view-up vector pair.
        """
        return super().get_sound_source_orientation_vu(sound_source_id)

    def set_sound_receiver_orientation_view_up(
        self,
        sound_receiver_id: int,
        view: Union[va_types.VAVector, list[float], tuple[float, float, float]],
        up: Union[va_types.VAVector, list[float], tuple[float, float, float]],
    ):
        """Set the orientation of a sound receiver as a view-up vector pair.

        Alias for / same as [`set_sound_receiver_orientation_vu`][vapython.va.VA.set_sound_receiver_orientation_vu].

        Args:
            sound_receiver_id: The ID of the sound receiver to set the orientation of.
            view: The view vector of the sound receiver.
            up: The up vector of the sound receiver.

        Examples:
            >>> va.set_sound_receiver_orientation_view_up(
            ...     sound_receiver_id=1, view=[0, 0, -1], up=[0, 1, 0]
            ... )

            Here, the view vector in OpenGL coordinates is [0, 0, -1] which means the
            sound receiver is looking straight ahead.
            The up vector is pointing upwards in the OpenGL coordinate system, this serves as the reference frame.
        """
        super().set_sound_receiver_orientation_vu(sound_receiver_id, view, up)

    def set_sound_receiver_real_world_position_orientation_view_up(
        self,
        sound_receiver_id: int,
        position: Union[va_types.VAVector, list[float], tuple[float, float, float]],
        view: Union[va_types.VAVector, list[float], tuple[float, float, float]],
        up: Union[va_types.VAVector, list[float], tuple[float, float, float]],
    ):
        """Set the real-world position and orientation of a sound receiver.

        Alias for / same as
        [`set_sound_receiver_real_world_position_orientation_vu`][vapython.va.VA.set_sound_receiver_real_world_position_orientation_vu].

        This function is used to provide the crosstalk-cancellation module with the current position of the
        sound receivers head in the real-world.

        Note:
            Coordinates refer the to center of the head on the axis which goes through both ears.

        Args:
            sound_receiver_id: The ID of the sound receiver to set the real-world position and orientation of
            position: The real-world position of the sound receiver.
            view: The view vector of the sound receiver.
            up: The up vector of the sound receiver.

        Examples:
            >>> va.set_sound_receiver_real_world_position_orientation_view_up(
            ...     sound_receiver_id=1, position=[0, 1.7, 0], view=[0, 0, -1], up=[0, 1, 0]
            ... )

            Here, the real world origin is defined at the floor.
            Thus, the position of the sound receiver is 1.7 m above the floor.
            The view vector in OpenGL coordinates is [0, 0, -1] which means the
            sound receiver is looking straight ahead.
            The up vector is pointing upwards in the OpenGL coordinate system, this serves as the reference frame.
        """
        super().set_sound_receiver_real_world_position_orientation_vu(sound_receiver_id, position, view, up)

    def set_sound_source_orientation_view_up(
        self,
        sound_source_id: int,
        view: Union[va_types.VAVector, list[float], tuple[float, float, float]],
        up: Union[va_types.VAVector, list[float], tuple[float, float, float]],
    ):
        """Set the orientation of a sound source as a view-up vector pair.

        Alias for / same as [`set_sound_source_orientation_vu`][vapython.va.VA.set_sound_source_orientation_vu].

        Args:
            sound_source_id: The ID of the sound source to set the orientation of.
            view: The view vector of the sound source.
            up: The up vector of the sound source.

        Examples:
            >>> va.set_sound_source_orientation_view_up(
            ...     sound_source_id=1, view=[0, 0, -1], up=[0, 1, 0]
            ... )

            Here, the view vector in OpenGL coordinates is [0, 0, -1] which means the
            sound source main axis of radiation is straight ahead.
            The up vector is pointing upwards in the OpenGL coordinate system, this serves as the reference frame.
        """
        super().set_sound_source_orientation_vu(sound_source_id, view, up)

    def get_homogeneous_medium_shift_parameters(self, parameters: va_types.VAStruct):
        return super().get_homogeneous_medium_parameters(parameters)

    def set_homogeneous_medium_shift_parameters(self, parameters: va_types.VAStruct):
        super().set_homogeneous_medium_parameters(parameters)

    def close_timer(self) -> None:
        """Close the timer.

        Primarily used for compatibility with Matlab.
        """
        self._timer_interval = -1
        self._timer_last_call = None

    def set_timer(self, interval: float) -> None:
        """Set the timer interval in seconds."""
        if interval < 0:
            msg = "Interval must be greater or equal to 0."
            raise ValueError(msg)

        self._timer_interval = interval
        self._timer_last_call = time.perf_counter_ns()

    def wait_for_timer(self) -> None:
        """Wait for the timer interval.

        This function will wait until the timer interval is reached and return.
        The timer is reset and a new interval is started.
        This can be used to adapt the VA scene in a time-controlled manner for example for constant speed movements.

        The interval is set by [`set_timer()`][vapython.va.VA.set_timer] in seconds.

        The accuracy of the timer depends on the system but should be not lower than 1 ms.

        As this is implemented with a busy loop, use with caution.

        Examples:
            We want to move a sound source with a constant speed. We can use the timer to update the position.
            Here we move the sound source with 1 m/s and update the position every 0.1 seconds.

            >>> source_id = va.create_sound_source("source1")
            >>> timer_interval = 0.1
            >>> va.set_timer(timer_interval)
            >>> delta_distance = 1 * timer_interval
            >>> va.set_sound_source_position(source_id, [0, 0, 0])
            >>> while True:
            ...     current_position = va.get_sound_source_position(source_id)
            ...     va.set_sound_source_position(
            ...         source_id[current_position[0] + delta_distance, 0, 0]
            ...     )
            ...     va.wait_for_timer()
        """
        if not self._timer_interval:
            warnings.warn(
                "Timer interval not set. Use `set_timer` to set the interval.",
                stacklevel=2,
            )
            return

        if not self._timer_last_call:
            return

        while time.perf_counter_ns() - self._timer_last_call < self._timer_interval * 1e9:
            pass

        self._timer_last_call = time.perf_counter_ns()

    def connect_tracker(
        self,
        server_ip: str,
        tracker: tracking.TrackingType = tracking.TrackingType.NatNet,
    ) -> None:
        """Connect to a tracking system.

        This function connects to a tracking system to track sound sources or receivers.
        The tracking system can be used to track the position and orientation of sound sources and receivers.

        Sources and receivers can be set as "tracked" via the respective `set_tracked_*` functions.
        These methods can be used before or after connecting to the tracking system.

        Via the `tracker` argument, the tracking system can be selected. Without the IHTATrackingPython package,
        only the NatNet tracking system is available.

        Args:
            server_ip: The IP address of the tracking server.
            tracker: The tracking system to use. Defaults to `NatNet`.

        Examples:
            This will connect to a NatNet tracking system running on the same computer and track a
            sound source with the second rigid body.

            >>> va.connect_tracker("127.0.0.1")
            >>> va.set_tracked_sound_source(sound_source, 1)
        """
        from vapython.tracking import Tracker

        self._tracker = Tracker(self, server_ip, tracker)

    def disconnect_tracker(self) -> None:
        """Disconnect from the tracking system."""
        if self._tracker is None:
            return
        self._tracker.disconnect()
        del self._tracker
        self._tracker = None

    def get_tracker_connected(self) -> bool:
        """Check if the tracker is connected."""
        return bool(self._tracker)

    def get_tracker_info(self) -> va_types.VAStruct:
        """Get information about the tracked objects.

        This includes what objects are tracked and their offsets.
        As well as if the tracking is connected.

        Returns:
            A dictionary with information about the tracked objects.
        """
        tracker_info: va_types.VAStruct = {"IsConnected": bool(self._tracker)}

        for tracker_id, tracker_data in self._tracker_data.items():
            concrete_tracker_data: va_types.VAStruct = {"TrackerID": tracker_id}

            if tracker_data.rotation_offset:
                concrete_tracker_data["RotationOffset"] = str(tracker_data.rotation_offset)

            if isinstance(tracker_data, tracking.SourceTrackingData):
                concrete_tracker_data["SourceID"] = tracker_data.source_id

                if tracker_data.position_offset:
                    concrete_tracker_data["PositionOffset"] = str(tracker_data.position_offset)

                tracker_info[f"TrackedSource{tracker_data.source_id}"] = concrete_tracker_data

            elif isinstance(tracker_data, tracking.ReceiverTrackingData):
                concrete_tracker_data["ReceiverID"] = tracker_data.receiver_id

                if tracker_data.position_offset:
                    concrete_tracker_data["PositionOffset"] = str(tracker_data.position_offset)

                tracker_info["TrackedReceiver"] = concrete_tracker_data

            elif isinstance(tracker_data, tracking.ReceiverRealWorldTrackingData):
                concrete_tracker_data["ReceiverID"] = tracker_data.receiver_id

                if tracker_data.position_offset:
                    concrete_tracker_data["PositionOffset"] = str(tracker_data.position_offset)

                tracker_info["TrackedRealWorldReceiver"] = concrete_tracker_data

            elif isinstance(tracker_data, tracking.ReceiverTorsoTrackingData):
                concrete_tracker_data["ReceiverID"] = tracker_data.receiver_id

                tracker_info["TrackedReceiverTorso"] = concrete_tracker_data

            elif isinstance(tracker_data, tracking.ReceiverRealWorldTorsoTrackingData):
                concrete_tracker_data["ReceiverID"] = tracker_data.receiver_id

                tracker_info["TrackedRealWorldReceiverTorso"] = concrete_tracker_data

        return tracker_info

    def set_tracked_sound_source(self, sound_source_id: int, tracker_id: int) -> None:
        """Set a sound source to be tracked by the tracking system.

        This function sets a sound source to be tracked by the tracking system.
        The sound source will be tracked with the given `tracker_id`.

        This method can be called before or after connecting to the tracking system.

        Args:
            sound_source_id: The ID of the sound source to track.
            tracker_id: The ID of the tracker to use.
        """
        self._tracker_data[tracker_id] = tracking.SourceTrackingData(source_id=sound_source_id)

    def set_tracked_sound_receiver(self, sound_receiver_id: int, tracker_id: int) -> None:
        """Set a sound receiver to be tracked by the tracking system.

        This function sets a sound receiver to be tracked by the tracking system.
        The sound receiver will be tracked with the given `tracker_id`.

        This method can be called before or after connecting to the tracking system.

        Args:
            sound_receiver_id: The ID of the sound receiver to track.
            tracker_id: The ID of the tracker to use.
        """
        self._tracker_data[tracker_id] = tracking.ReceiverTrackingData(receiver_id=sound_receiver_id)

    def set_tracked_sound_receiver_torso(self, sound_receiver_id: int, tracker_id: int) -> None:
        """Set a sound receiver torso to be tracked by the tracking system.

        This function sets a sound receiver torso to be tracked by the tracking system.
        The sound receiver torso will be tracked with the given `tracker_id`.

        The rotation of the torso will influence the HRIR selection.

        This method can be called before or after connecting to the tracking system.

        Args:
            sound_receiver_id: The ID of the sound receiver torso to track.
            tracker_id: The ID of the tracker to use.
        """
        self._tracker_data[tracker_id] = tracking.ReceiverTorsoTrackingData(receiver_id=sound_receiver_id)

    def set_tracked_real_world_sound_receiver(self, sound_receiver_id: int, tracker_id: int) -> None:
        """Set a real-world sound receiver to be tracked by the tracking system.

        This function sets a real-world sound receiver to be tracked by the tracking system.
        The real-world sound receiver will be tracked with the given `tracker_id`.

        This method can be called before or after connecting to the tracking system.

        Args:
            sound_receiver_id: The ID of the real-world sound receiver to track.
            tracker_id: The ID of the tracker to use.
        """
        self._tracker_data[tracker_id] = tracking.ReceiverRealWorldTrackingData(receiver_id=sound_receiver_id)

    def set_tracked_real_world_sound_receiver_torso(self, sound_receiver_id: int, tracker_id: int) -> None:
        """Set a real-world sound receiver torso to be tracked by the tracking system.

        This function sets a real-world sound receiver torso to be tracked by the tracking system.
        The real-world sound receiver torso will be tracked with the given `tracker_id`.

        The rotation of the torso will influence the HRIR selection.

        This method can be called before or after connecting to the tracking system.

        Args:
            sound_receiver_id: The ID of the real-world sound receiver torso to track.
            tracker_id: The ID of the tracker to use.
        """
        self._tracker_data[tracker_id] = tracking.ReceiverRealWorldTorsoTrackingData(receiver_id=sound_receiver_id)

    def set_tracked_sound_source_offset(
        self,
        sound_source_id: int,
        *,
        position_offset: Optional[Union[va_types.VAVector, list[float], tuple[float, float, float]]] = None,
        orientation_offset: Optional[
            Union[va_types.VAQuaternion, list[float], tuple[float, float, float, float]]
        ] = None,
    ) -> None:
        """Set the offset for a tracked sound source.

        The orientation offset is applied directly to the orientation of the sound source.
        The position offset is first rotated by the orientation of the sound source and then applied.

        Args:
            sound_source_id: The ID of the sound source to set the offset for.
            position_offset: The position offset to set.
            orientation_offset: The orientation offset to set.
        """
        for data in self._tracker_data.values():
            if isinstance(data, tracking.SourceTrackingData) and data.source_id == sound_source_id:
                if position_offset is not None:
                    data.position_offset = position_offset
                if orientation_offset is not None:
                    data.rotation_offset = orientation_offset
                break

    def set_tracked_sound_receiver_offset(
        self,
        sound_receiver_id: int,
        *,
        position_offset: Optional[Union[va_types.VAVector, list[float], tuple[float, float, float]]] = None,
        orientation_offset: Optional[
            Union[va_types.VAQuaternion, list[float], tuple[float, float, float, float]]
        ] = None,
    ) -> None:
        """Set the offset for a tracked sound receiver.

        This can be useful if the origin of the rigid body does not align with the acoustic center.
        For example, a tracker mounted on top of a head should still have the acoustic center at ear height.

        The orientation offset is applied directly to the orientation of the sound receiver.
        The position offset is first rotated by the orientation of the sound receiver and then applied.

        Args:
            sound_receiver_id: The ID of the sound receiver to set the offset for.
            position_offset: The position offset to set.
            orientation_offset: The orientation offset to set.
        """
        for data in self._tracker_data.values():
            if isinstance(data, tracking.ReceiverTrackingData) and data.receiver_id == sound_receiver_id:
                if position_offset is not None:
                    data.position_offset = position_offset
                if orientation_offset is not None:
                    data.rotation_offset = orientation_offset
                break

    def set_tracked_sound_receiver_torso_offset(
        self,
        sound_receiver_id: int,
        *,
        orientation_offset: Optional[
            Union[va_types.VAQuaternion, list[float], tuple[float, float, float, float]]
        ] = None,
    ) -> None:
        """Set the offset for a tracked sound receiver torso.

        This can be useful if the origin of the rigid body does not align with the acoustic center.

        Args:
            sound_receiver_id: The ID of the sound receiver torso to set the offset for.

            orientation_offset: The orientation offset to set.
        """
        for data in self._tracker_data.values():
            if isinstance(data, tracking.ReceiverTorsoTrackingData) and data.receiver_id == sound_receiver_id:
                if orientation_offset is not None:
                    data.rotation_offset = orientation_offset
                break

    def set_tracked_real_world_sound_receiver_offset(
        self,
        sound_receiver_id: int,
        *,
        position_offset: Optional[Union[va_types.VAVector, list[float], tuple[float, float, float]]] = None,
        orientation_offset: Optional[
            Union[va_types.VAQuaternion, list[float], tuple[float, float, float, float]]
        ] = None,
    ) -> None:
        """Set the offset for a tracked real-world sound receiver.

        This can be useful if the origin of the rigid body does not align with the acoustic center.
        For example, a tracker mounted on top of a head should still have the acoustic center at ear height.

        The orientation offset is applied directly to the orientation of the sound receiver.
        The position offset is first rotated by the orientation of the sound receiver and then applied.

        Args:
            sound_receiver_id: The ID of the real-world sound receiver to set the offset for.
            position_offset: The position offset to set.
            orientation_offset: The orientation offset to set.
        """
        for data in self._tracker_data.values():
            if isinstance(data, tracking.ReceiverRealWorldTrackingData) and data.receiver_id == sound_receiver_id:
                if position_offset is not None:
                    data.position_offset = position_offset
                if orientation_offset is not None:
                    data.rotation_offset = orientation_offset
                break

    def set_tracked_real_world_sound_receiver_torso_offset(
        self,
        sound_receiver_id: int,
        *,
        orientation_offset: Optional[
            Union[va_types.VAQuaternion, list[float], tuple[float, float, float, float]]
        ] = None,
    ) -> None:
        """Set the offset for a tracked real-world sound receiver torso.

        This can be useful if the origin of the rigid body does not align with the acoustic center.

        Args:
            sound_receiver_id: The ID of the real-world sound receiver torso to set the offset for.
            orientation_offset: The orientation offset to set.
        """
        for data in self._tracker_data.values():
            if isinstance(data, tracking.ReceiverRealWorldTorsoTrackingData) and data.receiver_id == sound_receiver_id:
                if orientation_offset is not None:
                    data.rotation_offset = orientation_offset
                break

    def _apply_tracking(
        self,
        tracker_id: int,
        position: tuple[float, float, float],
        orientation: tuple[float, float, float, float],
    ):
        """Internal function to apply the tracking data to the VA server.

        Args:
            tracker_id: The ID of the tracker to apply the data for.
            position: The position to apply.
            orientation: The orientation to apply.
        """
        if tracker_id not in self._tracker_data:
            return

        data = self._tracker_data[tracker_id]

        if data.rotation_offset:
            orientation_offset = Rotation.from_quat(data.rotation_offset, scalar_first=False)
            orientation_quat = Rotation.from_quat(orientation, scalar_first=False)

            orientation = tuple((orientation_offset * orientation_quat).as_quat())

        if data.position_offset:
            orientation_quat = Rotation.from_quat(orientation, scalar_first=False)

            position_offset = orientation_quat.apply(data.position_offset)

            position = tuple([position[i] + position_offset[i] for i in range(3)])

        def head_above_torso_orientation(
            orientation: tuple[float, float, float, float], receiver_id
        ) -> tuple[float, float, float, float]:
            """Calculate the orientation of the head above the torso."""
            orientation_quat = Rotation.from_quat(orientation, scalar_first=False)

            head_quat = Rotation.from_quat(self.get_sound_receiver_orientation(receiver_id), scalar_first=False)
            orientation_quat = orientation_quat.inv() * head_quat
            return tuple(orientation_quat.as_quat())

        if isinstance(data, tracking.SourceTrackingData):
            self.set_sound_source_pose(data.source_id, position, orientation)
        elif isinstance(data, tracking.ReceiverTrackingData):
            self.set_sound_receiver_pose(data.receiver_id, position, orientation)
        elif isinstance(data, tracking.ReceiverRealWorldTrackingData):
            self.set_sound_receiver_real_world_pose(data.receiver_id, position, orientation)
        elif isinstance(data, tracking.ReceiverTorsoTrackingData):
            # TODO: check if this is correct
            orientation = head_above_torso_orientation(orientation, data.receiver_id)
            self.set_sound_receiver_head_above_torso_orientation(data.receiver_id, orientation)
        elif isinstance(data, tracking.ReceiverRealWorldTorsoTrackingData):
            # TODO: check if this is correct
            orientation = head_above_torso_orientation(orientation, data.receiver_id)
            self.set_sound_receiver_real_world_head_above_torso_orientation(data.receiver_id, orientation)
