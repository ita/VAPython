from dataclasses import dataclass
from typing import Optional, Union

import vapython._types as va_types


@dataclass
class BaseTrackingData:
    """Base class for tracking data."""

    position_offset: Optional[Union[va_types.VAVector, list[float], tuple[float, float, float]]] = None
    rotation_offset: Optional[Union[va_types.VAQuaternion, list[float], tuple[float, float, float, float]]] = None


@dataclass
class BaseReceiverTrackingData(BaseTrackingData):
    """Base class for receiver tracking data."""

    receiver_id: int = -1


@dataclass
class ReceiverTrackingData(BaseReceiverTrackingData):
    """Class for receiver tracking data."""


@dataclass
class ReceiverTorsoTrackingData(BaseReceiverTrackingData):
    """Class for receiver torso tracking data."""


@dataclass
class ReceiverRealWorldTrackingData(BaseReceiverTrackingData):
    """Class for receiver real-world tracking data."""


@dataclass
class ReceiverRealWorldTorsoTrackingData(BaseReceiverTrackingData):
    """Class for receiver real-world torso tracking data."""


@dataclass
class SourceTrackingData(BaseTrackingData):
    """Class for source tracking data."""

    source_id: int = -1
