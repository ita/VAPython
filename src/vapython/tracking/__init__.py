# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

from __future__ import annotations

import warnings
from enum import Enum
from typing import TYPE_CHECKING

from vapython.tracking.data_types import *  # noqa F403

ihta_tracking_available = True
try:
    import IHTATrackingPython as IHTATracking  # type: ignore
except ImportError:
    ihta_tracking_available = False
    warnings.warn(
        "IHTATrackingPython not found. Falling back to NatNet only.",
        ImportWarning,
        stacklevel=2,
    )

if not ihta_tracking_available:
    import vapython.tracking.NatNetClient as NatNetTracking


if TYPE_CHECKING:
    from vapython import VA


class TrackingType(Enum):
    """Enumeration for the tracking types."""

    NatNet = 1
    ART = 2


class Tracker:
    """Wrapper to interface with different tracking systems.

    Without the IHTATrackingPython library, only NatNet tracking is available.
    """

    def __init__(self, va_instance: VA, server_ip: str, tracker: TrackingType):
        """Initialize the tracker.

        Args:
            va_instance: The VA instance.
            server_ip: The IP address of the tracking server.
            tracker: The tracking type.
        """
        if tracker == TrackingType.ART and not ihta_tracking_available:
            msg = "IHTATrackingPython not found. Cannot use ART tracking."
            raise RuntimeError(msg)

        if ihta_tracking_available:
            ihta_tracking_type = (
                IHTATracking.Tracker.Type.NATNET if tracker == TrackingType.NatNet else IHTATracking.Tracker.Type.ART
            )
            self._tracker = IHTATracking.Tracker(ihta_tracking_type, server_ip)

            self._tracker.registerCallback(self.ihta_callback)
        else:
            self._tracker = NatNetTracking.NatNetClient()

            self._tracker.serverIPAddress = server_ip
            self._tracker.newFrameListener = self.natnet_frame_callback  # type: ignore
            self._tracker.rigidBodyListener = self.natnet_callback  # type: ignore

            self._tracker.run()

        self._va_instance = va_instance

    def __del__(self):
        """Destructor."""
        if ihta_tracking_available:
            pass
        else:
            self._tracker.stop()
            del self._tracker

    def disconnect(self):
        """Disconnect from the tracking."""
        if ihta_tracking_available:
            pass
        else:
            self._tracker.stop()

    def ihta_callback(self, data: list[IHTATracking.TrackingDataPoint]):
        """Callback for the IHTA tracking.

        TODO: Implement this method.
        """

    def natnet_callback(self, new_id, position, rotation):
        """Callback for the NatNet tracking.

        The callback is called for each rigid body, each frame.
        The position is given as a tuple of 3 floats (x, y, z).
        The rotation is given as a tuple of 4 floats (x, y, z, w) aka quaternion.
        Args:
            new_id: The new ID.
            position: The position.
            rotation: The rotation.
        """
        self._va_instance._apply_tracking(new_id, position, rotation)  # noqa SLF001

    def natnet_frame_callback(self, *_):
        """Callback for the NatNet tracking for each frame.

        This is not used in the current implementation.
        """
