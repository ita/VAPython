# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

"""Type definitions for VAPython.

This mainly includes the type definitions for the VA data structures in pure Python.
These include the `VAStruct`, `VAVector`, and `VAQuaternion` types which map to a dict and two named tuples, respectively.
"""

from typing import NamedTuple, Union

VAStructValue = Union[bool, int, float, str, bytes, list[float], "VAStruct"]

VAStruct = dict[str, VAStructValue]
"""Represents a structure in VAPython.

Used to communicate complex data and messages to and from the VAServer.
"""


class VAVector(NamedTuple):
    """Represents a vector in three-dimensional space.

    Note:
        In VAPython, vectors follow the OpenGL coordinate system, i.e.,
        the x-axis points to the right, the y-axis points upwards, and the z-axis points towards the viewer.

    Attributes:
        x (float): The x-coordinate of the vector.
        y (float): The y-coordinate of the vector.
        z (float): The z-coordinate of the vector.
    """

    x: float
    y: float
    z: float


class VAQuaternion(NamedTuple):
    """
    Represents a quaternion in a 3D space.

    Attributes:
        x (float): The x-coordinate of the quaternion.
        y (float): The y-coordinate of the quaternion.
        z (float): The z-coordinate of the quaternion.
        w (float): The w-coordinate of the quaternion.
    """

    x: float
    y: float
    z: float
    w: float
