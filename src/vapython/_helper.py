# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import configparser
import re
import shutil
import subprocess
import warnings
from importlib import metadata
from pathlib import Path
from typing import Optional

from platformdirs import user_config_dir

possible_auralization_modes = {
    "Nothing": (0, "Nothing"),
    "DS": (1 << 0, "Direct Sound"),
    "ER": (1 << 1, "Early Reflections"),
    "DD": (1 << 2, "Diffuse Decay"),
    "SD": (1 << 3, "Source Directivity"),
    "MA": (1 << 4, "Medium Absorption"),
    "TV": (1 << 5, "Atmospheric Temporal Variations"),
    "SC": (1 << 6, "Scattering"),
    "DF": (1 << 7, "Diffraction"),
    "NF": (1 << 8, "Near-field Effects"),
    "DP": (1 << 9, "Doppler Effect"),
    "SL": (1 << 10, "Spherical Spreading Loss"),
    "TR": (1 << 11, "Transmission Loss"),
    "AB": (1 << 12, "Absorption"),
}
"""
Dictionary containing all possible auralization modes.

Auralization modes allow to enable or disable specific effects in the auralization process during runtime.
A more in-depth explanation of each auralization mode can be found [here](https://www.virtualacoustics.de/VA/documentation/control/#global-auralization-mode).
"""

default_auralization_mode = sum(
    [
        possible_auralization_modes["DS"][0],
        possible_auralization_modes["SD"][0],
        possible_auralization_modes["MA"][0],
        possible_auralization_modes["DP"][0],
        possible_auralization_modes["SL"][0],
        possible_auralization_modes["TR"][0],
        possible_auralization_modes["AB"][0],
    ]
)
"""
Set of default auralization modes.

For more information see [`possible_auralization_modes`][vapython._helper.possible_auralization_modes] and the
[VA documentation](https://www.virtualacoustics.de/VA/documentation/control/#global-auralization-mode).
"""


def parse_aura_mode_str(input_string: str, aura_mode: int = default_auralization_mode) -> int:
    """Parses the input string and returns the corresponding auralization mode as an integer.

    The input string can contain a list of auralization modes separated by commas. Each auralization mode can be
    prefixed with a "+" or "-" sign to enable or disable the mode, respectively.
    The aura mode can also be set to "*" to enable all aura modes.

    Examples:
        >>> parse_aura_mode_str("DS,ER,DD")
        7
        >>> parse_aura_mode_str("+DS,-ER,DD")
        5
        >>> parse_aura_mode_str("*")
        8191
        >>> parse_aura_mode_str("default")
        7705
        >>> parse_aura_mode_str("")
        0

    Args:
        input_string: The input string representing the auralization mode.
        aura_mode (optional): The default auralization mode. Defaults to `default_aura_mode`.

    Returns:
        The auralization mode as an integer.

    Raises:
        ValueError: If an invalid auralization mode is encountered in the input string.
    """
    if input_string == "":
        return 0  # No aura mode

    if input_string == "*":
        return sum(m[0] for m in possible_auralization_modes.values())  # All aura modes

    if input_string.lower() == "default":
        return default_auralization_mode

    split_input = re.findall(r"(?:(\+|-)([A-Z]{2})(?:,|$))+?", input_string, re.IGNORECASE)

    enable = []
    disable = []

    for sign, mode in split_input:
        # check if mode is valid
        if mode not in possible_auralization_modes:
            msg = f"Invalid aura mode: {mode}"
            raise ValueError(msg)

        if sign == "+":
            enable.append(possible_auralization_modes[mode][0])
        else:
            disable.append(possible_auralization_modes[mode][0])

    aura_mode = aura_mode | sum(enable)
    aura_mode = aura_mode & ~sum(disable)

    return aura_mode  # noqa: RET504


def convert_aura_mode_to_str(aura_mode: int, *, short_form: bool = True) -> str:
    """Converts an auralization mode value to a string representation.

    Auralization modes are represented as a bitfield. This function converts the bitfield to a string representation.
    Some special values are also supported:
    - 0: "Nothing"
    - All modes: "All"
    - Default modes, see [`default_aura_mode`][vapython._helpers.default_aura_mode]: "Default"

    See also [`possible_auralization_modes`][vapython._helpers.possible_auralization_modes] for all possible aura modes.

    Examples:
        >>> convert_aura_mode_to_str(0)
        "Nothing"
        >>> convert_aura_mode_to_str(0b1111111111111)
        "All"
        >>> convert_aura_mode_to_str(0b1111111111111, short_form=False)
        "Direct Sound,Early Reflections,Diffuse Decay,Source Directivity,Medium Absorption,...
        Atmospheric Temporal Variations,Scattering,Diffraction,Near-field Effects,Doppler Effect,...
        Spherical Spreading Loss,Transmission Loss,Absorption"
        >>> convert_aura_mode_to_str(7)
        "DS,ER,DD"

    Args:
        aura_mode: The auralization mode value to convert.
        short_form (optional): Whether to use the short form or the full name for each aura mode. Defaults to True.

    Returns:
        The string representation of the auralization mode value.

    """
    if aura_mode == 0 and not short_form:
        return "Nothing"

    if aura_mode == sum(m[0] for m in possible_auralization_modes.values()) and not short_form:
        return "All"

    if aura_mode == default_auralization_mode and not short_form:
        return "Default"

    aura_modes = []

    for mode, (value, name) in possible_auralization_modes.items():
        if aura_mode & value:
            if short_form:
                aura_modes.append(mode)
            else:
                aura_modes.append(name)

    return ",".join(aura_modes)


def find_server_executable() -> Optional[Path]:
    """Finds the a VAServer executable.

    This function first checks if an executable path is stored in the user's configuration directory.
    If not, the function checks if the VAServer executable is available in the system's PATH.
    If not found, it opens a file dialog to allow the user to manually select the VAServer executable.
    A valid executable path is stored in the user's configuration directory for future use.

    This function also checks if the version of the VAServer executable matches the version of the VAPython package
    and issues a warning if a mismatch is detected. This however does not mean, that the VAServer executable is not
    compatible with the VAPython package.

    Returns:
        Union[Path, None]: The path to the VAServer executable if found, None otherwise.
    """

    config_dir = Path(user_config_dir("vapython", version=metadata.version("vapython")))
    config_file = config_dir / "vapython.cfg"

    executable: Optional[Path] = None
    if config_file.exists():
        config = configparser.ConfigParser()
        config.read(config_file)
        executable = Path(config["executable"]["path"])

    if executable is None or not executable.is_file() or not executable.exists():
        executable_raw = shutil.which("VAServer")

        if executable_raw is not None:
            executable = Path(executable_raw)

        if executable is None:
            import tkinter as tk
            from tkinter import filedialog

            root = tk.Tk()
            root.withdraw()
            root.attributes("-topmost", 1)

            print(  # noqa: T201
                "NOTE: A window has opened for specifying the path to the 'VAServer.exe'. This should be located in the `bin` subfolder of the respective VA directory."
            )
            executable_raw = filedialog.askopenfilename(
                title="Select VAServer executable",
                filetypes=[("VAServer executable", "*.exe")],
                initialfile="VAServer.exe",
            )

            if not executable_raw:
                return None

            executable = Path(executable_raw)

        result = subprocess.run([str(executable), "--version"], capture_output=True, check=False)
        exe_version_raw = result.stdout.decode().strip()
        exe_version = re.split(r"(\d{4}).(\D)", exe_version_raw)
        major_version = exe_version[1]
        minor_version = exe_version[2]
        minor_version = ord(minor_version) - 97
        exe_version_final = f"{major_version}.{minor_version}"

        if metadata.version("vapython") != exe_version_final:
            warnings.warn(
                (
                    "Version mismatch between VAPython package and VAServer executable. "
                    f"Python package version: {metadata.version('vapython')}, executable version: {exe_version_final}"
                ),
                stacklevel=2,
            )

        config = configparser.ConfigParser()
        config["executable"] = {"path": str(executable)}
        config_dir.mkdir(parents=True, exist_ok=True)
        with open(config_file, "w") as config_file_handle:
            config.write(config_file_handle)

    return Path(executable)
