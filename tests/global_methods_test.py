# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random
from pathlib import Path

import pytest
from betterproto.lib.google import protobuf

import vapython._helper as va_helper
import vapython.vanet._helper as helper
import vapython.vanet._vanet_grpc as va_grpc
from vapython import VA

from .utils import random_grpc_struct, random_string, random_struct

random_floats = [-1.0, 0.0, 1.0, 3.141]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random_string(5),
            random_string(5),
            random_string(5),
        )
        for _ in range(5)
    ]
    + [("", "", "", "")],
)
async def test_get_version_info(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_version_info", return_value=va_grpc.VersionInfo(*test_input), autospec=True)

    info = va.get_version_info()

    assert service.get_version_info.called
    assert info.version == test_input[0]
    assert info.release_date == test_input[1]
    assert info.property_flags == test_input[2]
    assert info.comments == test_input[3]


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", [True, False])
async def test_set_output_muted(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "set_output_muted", return_value=protobuf.Empty(), autospec=True)

    va.set_output_muted(muted=test_input)

    service.set_output_muted.assert_called_once_with(va_grpc.SetOutputMutedRequest(muted=test_input))


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", [True, False])
async def test_get_output_muted(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_output_muted", return_value=protobuf.BoolValue(value=test_input), autospec=True)

    ret_val = va.get_output_muted()

    assert service.get_output_muted.called
    assert ret_val == test_input


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", [True, False])
async def test_set_input_muted(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "set_input_muted", return_value=protobuf.Empty(), autospec=True)

    va.set_input_muted(muted=test_input)

    service.set_input_muted.assert_called_once_with(va_grpc.SetInputMutedRequest(muted=test_input))


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", [True, False])
async def test_get_input_muted(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_input_muted", return_value=protobuf.BoolValue(value=test_input), autospec=True)

    ret_val = va.get_input_muted()

    assert service.get_input_muted.called
    assert ret_val == test_input


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random_struct(),
        )
        for _ in range(5)
    ],
)
async def test_call_module(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(
        service, "call_module", return_value=helper.convert_struct_to_vanet(test_input[1]), autospec=True
    )

    ret_val = va.call_module(module_name=test_input[0], module_parameters=test_input[1])

    service.call_module.assert_called_once_with(
        va_grpc.CallModuleRequest(
            module_name=test_input[0], module_parameters=helper.convert_struct_to_vanet(test_input[1])
        )
    )
    assert test_input[1] == ret_val


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        va_grpc.VaModuleInfos(
            module_infos=[
                va_grpc.VaModuleInfosModuleInfo(
                    name=random_string(5), description=random_string(10), id=random.randint(0, 5)
                )
                for _ in range(random.randint(0, 5))
            ]
        )
        for _ in range(5)
    ],
)
async def test_get_modules(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_modules", return_value=test_input, autospec=True)

    ret_val = va.get_modules()

    assert service.get_modules.called
    assert test_input == ret_val


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", random_floats)
async def test_set_output_gain(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "set_output_gain", return_value=protobuf.Empty(), autospec=True)

    va.set_output_gain(gain=test_input)

    service.set_output_gain.assert_called_once_with(va_grpc.SetOutputGainRequest(gain=test_input))


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", random_floats)
async def test_get_output_gain(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_output_gain", return_value=protobuf.DoubleValue(value=test_input), autospec=True)

    ret_val = va.get_output_gain()

    assert service.get_output_gain.called
    assert ret_val == test_input


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", random_floats)
async def test_set_input_gain(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "set_input_gain", return_value=protobuf.Empty(), autospec=True)

    va.set_input_gain(gain=test_input)

    service.set_input_gain.assert_called_once_with(va_grpc.SetInputGainRequest(gain=test_input))


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", random_floats)
async def test_get_input_gain(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_input_gain", return_value=protobuf.DoubleValue(value=test_input), autospec=True)

    ret_val = va.get_input_gain()

    assert service.get_input_gain.called
    assert ret_val == test_input


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        ("+DS", 0),
        ("+DS,-ER", 0),
        ("+DS,-ER,-SD", 5),
        ("+DS,-ER,-SD", 8191),
    ],
)
async def test_set_global_auralization_mode(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "set_global_auralization_mode", return_value=protobuf.Empty(), autospec=True)
    mocker.patch.object(
        service, "get_global_auralization_mode", return_value=protobuf.Int32Value(value=test_input[1]), autospec=True
    )

    va.set_global_auralization_mode(test_input[0])

    service.set_global_auralization_mode.assert_called_once_with(
        va_grpc.SetGlobalAuralizationModeRequest(mode=va_helper.parse_aura_mode_str(*test_input))
    )
    assert service.get_global_auralization_mode.called


@pytest.mark.asyncio
@pytest.mark.parametrize("aura_mode", [0, 1, 4096, 8191, 5, 7, 10])
async def test_get_global_auralization_mode(mocked_connection, mocker, aura_mode):
    va, service = mocked_connection

    mocker.patch.object(
        service, "get_global_auralization_mode", return_value=protobuf.Int32Value(value=aura_mode), autospec=True
    )

    ret_val = va.get_global_auralization_mode()

    assert service.get_global_auralization_mode.called
    assert ret_val == va_helper.convert_aura_mode_to_str(aura_mode)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "core_state", [va_grpc.CoreStateState.CREATED, va_grpc.CoreStateState.READY, va_grpc.CoreStateState.FAIL]
)
async def test_get_server_state(mocked_connection, mocker, core_state):
    va, service = mocked_connection

    mocker.patch.object(service, "get_state", return_value=va_grpc.CoreState(core_state), autospec=True)

    ret_val = va.get_server_state()

    assert service.get_state.called
    assert ret_val == va_grpc.CoreState(core_state)


@pytest.mark.asyncio
async def test_reset(mocked_connection, mocker):
    va, service = mocked_connection

    mocker.patch.object(service, "reset", return_value=protobuf.Empty(), autospec=True)

    va.reset()

    assert service.reset.called


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "filter_conf",
    [True, False],
)
@pytest.mark.parametrize(
    "test_input",
    [random_grpc_struct() for _ in range(5)],
)
async def test_get_core_configuration(mocked_connection, mocker, filter_conf, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_core_configuration", return_value=test_input, autospec=True)

    ret_val = va.get_core_configuration(only_enabled=filter_conf)

    service.get_core_configuration.assert_called_once_with(va_grpc.GetCoreConfigurationRequest(filter_conf))
    assert ret_val == helper.convert_struct_from_vanet(test_input)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [random_grpc_struct() for _ in range(5)],
)
async def test_get_hardware_configuration(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_hardware_configuration", return_value=test_input, autospec=True)

    ret_val = va.get_hardware_configuration()

    assert service.get_hardware_configuration.called
    assert ret_val == helper.convert_struct_from_vanet(test_input)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
async def test_get_core_clock(mocked_connection, mocker, test_input):  # noqa: ARG001
    va, service = mocked_connection

    clock = random.uniform(0, 10e3)

    mocker.patch.object(service, "get_core_clock", return_value=protobuf.DoubleValue(clock), autospec=True)

    ret_val = va.get_core_clock()

    assert service.get_core_clock.called
    assert ret_val == clock


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
async def test_set_core_clock(mocked_connection, mocker, test_input):  # noqa: ARG001
    va, service = mocked_connection

    clock = random.uniform(0, 10e3)

    mocker.patch.object(service, "set_core_clock", return_value=protobuf.Empty(), autospec=True)

    va.set_core_clock(clock)

    service.set_core_clock.assert_called_once_with(va_grpc.SetCoreClockRequest(clock))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [True, False],
)
async def test_get_update_locked(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "get_update_locked", return_value=protobuf.BoolValue(test_input), autospec=True)

    ret_val = va.get_update_locked()

    assert service.get_update_locked.called
    assert ret_val == test_input


@pytest.mark.asyncio
async def test_lock_update(mocked_connection, mocker):
    va, service = mocked_connection

    mocker.patch.object(service, "lock_update", return_value=protobuf.Empty(), autospec=True)

    va.lock_update()

    assert service.lock_update.called


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
async def test_unlock_update(mocked_connection, mocker, test_input):  # noqa: ARG001
    va, service = mocked_connection

    scene_state = random.randint(-1, 100)

    mocker.patch.object(service, "unlock_update", return_value=protobuf.Int32Value(scene_state), autospec=True)

    ret_val = va.unlock_update()

    assert service.unlock_update.called
    assert ret_val == scene_state


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
@pytest.mark.parametrize(
    "input_type",
    [Path, str],
)
@pytest.mark.parametrize(
    "return_value",
    [True, False],
)
async def test_add_search_path(mocked_connection, mocker, test_input, input_type, return_value):
    va, service = mocked_connection

    mocker.patch.object(
        service, "call_module", return_value=helper.convert_struct_to_vanet({"pathvalid": return_value}), autospec=True
    )

    test_path = input_type("/".join([random_string(5) for _ in range(test_input)]))

    ret_val = va.add_search_path(test_path)

    service.call_module.assert_called_once_with(
        va_grpc.CallModuleRequest(
            module_name="VACore", module_parameters=helper.convert_struct_to_vanet({"addsearchpath": str(test_path)})
        )
    )
    assert ret_val == return_value


@pytest.mark.asyncio
async def test_shutdown_server(mocked_connection, mocker):
    va, service = mocked_connection

    mocker.patch.object(service, "call_module", return_value=helper.convert_struct_to_vanet({}), autospec=True)

    va.shutdown_server()

    service.call_module.assert_called_once_with(
        va_grpc.CallModuleRequest(
            module_name="VACore", module_parameters=helper.convert_struct_to_vanet({"shutdown": True})
        )
    )


@pytest.mark.asyncio
async def test_get_server_address(mocked_connection):
    va = VA()

    addr = va.get_server_address()

    assert addr is None

    va, _ = mocked_connection

    addr = va.get_server_address()

    assert addr == "localhost:12340"


def test_start_server(mocker):
    with pytest.raises(FileNotFoundError, match="Could not find valid VACore.ini file."):
        VA.start_server(config_ini_file="should_not_exist.ini", va_server_path=__file__)

    with pytest.raises(FileNotFoundError, match="Could not find valid VAServer executable."):
        VA.start_server(config_ini_file="should_not_exist.ini", va_server_path="should_not_exist.exe")

    mocker.patch("vapython._helper.find_server_executable", return_value=Path(__file__), autospec=True)

    with pytest.raises(FileNotFoundError, match="Could not find valid VACore.ini file."):
        VA.start_server(config_ini_file="should_not_exist.ini")

    with pytest.raises(FileNotFoundError, match="Could not find valid VACore.ini file."):
        VA.start_server()

    mocker.patch("vapython._helper.find_server_executable", return_value=Path())

    with pytest.raises(FileNotFoundError, match="Could not find valid VAServer executable."):
        VA.start_server()

    import subprocess

    mocker.patch("subprocess.Popen", return_value=None, autospec=True)

    VA.start_server(config_ini_file=Path(__file__), va_server_path=Path(__file__))

    subprocess.Popen.assert_called_once_with(
        [
            Path(__file__),
            "--config",
            Path(__file__),
            "--server-address",
            "localhost:12340",
            "--remote",
        ],
        creationflags=subprocess.CREATE_NEW_CONSOLE,
    )

    VA.start_server(
        config_ini_file=Path(__file__), va_server_path=Path(__file__), extra_args=["foo", "bar"], dedicated_window=False
    )

    subprocess.Popen.assert_called_with(
        [
            Path(__file__),
            "--config",
            Path(__file__),
            "--server-address",
            "localhost:12340",
            "--remote",
            "foo",
            "bar",
        ],
    )
