# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0


import pytest

from vapython._types import VAQuaternion, VAStruct, VAVector
from vapython.vanet import _helper
from vapython.vanet._vanet_grpc import Quaternion as VanetQuaternion
from vapython.vanet._vanet_grpc import SampleBuffer as VanetSampleBuffer
from vapython.vanet._vanet_grpc import Struct as VanetStruct
from vapython.vanet._vanet_grpc import Value as VanetStructValue
from vapython.vanet._vanet_grpc import Vector3 as VanetVector


def test_convert_struct_to_vanet():
    input_struct = {
        "bool_field": True,
        "int_field": 10,
        "float_field": 3.14,
        "str_field": "Hello",
        "nested_struct_field": {"nested_bool_field": False, "nested_int_field": 20},
        "bytes_field": b"\x00\x01\x02",
        "buffer_field": [1.0, 2.0, 3.0],
    }

    expected_output = VanetStruct()
    expected_output.fields["bool_field"] = VanetStructValue(bool_value=True)
    expected_output.fields["int_field"] = VanetStructValue(integer_value=10)
    expected_output.fields["float_field"] = VanetStructValue(double_value=3.14)
    expected_output.fields["str_field"] = VanetStructValue(string_value="Hello")
    expected_output.fields["nested_struct_field"] = VanetStructValue(struct_value=VanetStruct())
    expected_output.fields["nested_struct_field"].struct_value.fields["nested_bool_field"] = VanetStructValue(
        bool_value=False
    )
    expected_output.fields["nested_struct_field"].struct_value.fields["nested_int_field"] = VanetStructValue(
        integer_value=20
    )
    expected_output.fields["bytes_field"] = VanetStructValue(data_value=b"\x00\x01\x02")
    expected_output.fields["buffer_field"] = VanetStructValue(buffer_value=VanetSampleBuffer([1.0, 2.0, 3.0]))

    assert _helper.convert_struct_to_vanet(input_struct) == expected_output


def test_convert_struct_to_vanet_invalid_input():
    with pytest.raises(TypeError, match="input_struct must be of type VAStruct or dict"):
        _helper.convert_struct_to_vanet("invalid_input")

    with pytest.raises(TypeError, match="input_struct must be of type VAStruct or dict"):
        _helper.convert_struct_to_vanet(1)

    with pytest.raises(ValueError, match="Unknown type of value in struct!"):
        _helper.convert_struct_to_vanet({"invalid_field": [1, 2, 3]})


def test_convert_struct_to_vanet_correct_input():
    data: VAStruct = {}
    _helper.convert_struct_to_vanet(data)  # should not raise an exception
    _helper.convert_struct_to_vanet({})  # should not raise an exception


def test_convert_struct_from_vanet():
    input_struct = VanetStruct()
    input_struct.fields["key1"] = VanetStructValue(bool_value=True)
    input_struct.fields["key2"] = VanetStructValue(integer_value=10)
    input_struct.fields["key3"] = VanetStructValue(double_value=3.14)
    input_struct.fields["key4"] = VanetStructValue(string_value="hello")

    nested_struct = VanetStruct()
    nested_struct.fields["nested_key"] = VanetStructValue(integer_value=20)
    input_struct.fields["key5"] = VanetStructValue(struct_value=nested_struct)

    input_struct.fields["key6"] = VanetStructValue(data_value=b"\x00\x01\x02")
    input_struct.fields["key7"] = VanetStructValue(buffer_value=VanetSampleBuffer([1.0, 2.0, 3.0]))

    output_struct = _helper.convert_struct_from_vanet(input_struct)

    assert output_struct == {
        "key1": True,
        "key2": 10,
        "key3": 3.14,
        "key4": "hello",
        "key5": {"nested_key": 20},
        "key6": b"\x00\x01\x02",
        "key7": [1.0, 2.0, 3.0],
    }


def test_convert_struct_from_vanet_invalid_inputs():
    with pytest.raises(TypeError, match="input_struct must be of type VanetStruct"):
        _helper.convert_struct_from_vanet("invalid_input")

    with pytest.raises(TypeError, match="input_struct must be of type VanetStruct"):
        _helper.convert_struct_from_vanet(1)

    data = VanetStruct()
    data.fields["key"] = VanetStructValue()
    with pytest.raises(ValueError, match="Value in struct not set!"):
        _helper.convert_struct_from_vanet(data)


def test_convert_vector_to_vanet_with_va_vector():
    input_vector = VAVector(x=1.0, y=2.0, z=3.0)
    expected_output = VanetVector(x=1.0, y=2.0, z=3.0)
    assert _helper.convert_vector_to_vanet(input_vector) == expected_output


def test_convert_vector_to_vanet_with_list():
    input_vector = [4.0, 5.0, 6.0]
    expected_output = VanetVector(x=4.0, y=5.0, z=6.0)
    assert _helper.convert_vector_to_vanet(input_vector) == expected_output


def test_convert_vector_to_vanet_with_tuple():
    input_vector = (7.0, 8.0, 9.0)
    expected_output = VanetVector(x=7.0, y=8.0, z=9.0)
    assert _helper.convert_vector_to_vanet(input_vector) == expected_output


def test_convert_vector_to_vanet_with_invalid_input():
    input_vector = "invalid"
    with pytest.raises(ValueError, match="Vector must be of type VAVector, list or tuple"):
        _helper.convert_vector_to_vanet(input_vector)


def test_convert_vector_to_vanet_with_invalid_length():
    input_vector = [1.0, 2.0]
    with pytest.raises(ValueError, match="Vector must be of length 3"):
        _helper.convert_vector_to_vanet(input_vector)


def test_convert_vector_from_vanet():
    input_vector = VanetVector(x=1, y=2, z=3)
    expected_output = VAVector(x=1, y=2, z=3)
    assert _helper.convert_vector_from_vanet(input_vector) == expected_output


def test_convert_quaternion_to_vanet_with_va_quaternion():
    input_quaternion = VAQuaternion(x=1.0, y=2.0, z=3.0, w=4.0)
    expected_output = VanetQuaternion(x=1.0, y=2.0, z=3.0, w=4.0)
    assert _helper.convert_quaternion_to_vanet(input_quaternion) == expected_output


def test_convert_quaternion_to_vanet_with_list():
    input_quaternion = [1.0, 2.0, 3.0, 4.0]
    expected_output = VanetQuaternion(x=1.0, y=2.0, z=3.0, w=4.0)
    assert _helper.convert_quaternion_to_vanet(input_quaternion) == expected_output


def test_convert_quaternion_to_vanet_with_tuple():
    input_quaternion = (1.0, 2.0, 3.0, 4.0)
    expected_output = VanetQuaternion(x=1.0, y=2.0, z=3.0, w=4.0)
    assert _helper.convert_quaternion_to_vanet(input_quaternion) == expected_output


def test_convert_quaternion_to_vanet_with_invalid_input():
    input_quaternion = "invalid"
    with pytest.raises(ValueError, match="Quaternion must be of type VAQuaternion, list or tuple"):
        _helper.convert_quaternion_to_vanet(input_quaternion)


def test_convert_quaternion_to_vanet_with_invalid_length():
    input_quaternion = [1.0, 2.0]
    with pytest.raises(ValueError, match="Quaternion must be of length 4"):
        _helper.convert_quaternion_to_vanet(input_quaternion)


def test_convert_quaternion_from_vanet():
    input_quaternion = VanetQuaternion(x=1, y=2, z=3, w=4)
    expected_output = VAQuaternion(x=1, y=2, z=3, w=4)
    assert _helper.convert_quaternion_from_vanet(input_quaternion) == expected_output
