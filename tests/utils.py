# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random
import string

import vapython.vanet._helper as helper
from vapython._types import VAStruct
from vapython.vanet._vanet_grpc import Struct as VanetStruct


def random_string(length: int) -> str:
    return "".join(random.choice(string.ascii_letters) for _ in range(length))


def random_struct() -> VAStruct:
    struct: VAStruct = {}

    struct["bool"] = random.choice([True, False])
    struct["int"] = random.randint(-100, 100)
    struct["double"] = random.uniform(-100, 100)
    struct["string"] = random_string(5)

    nested_struct: VAStruct = {}
    nested_struct["bytes"] = b"\x00\x01\x02"
    nested_struct["buffer"] = [random.uniform(-100, 100) for _ in range(random.randint(0, 5))]

    struct["struct"] = nested_struct

    return struct


def random_grpc_struct() -> VanetStruct:
    return helper.convert_struct_to_vanet(random_struct())
