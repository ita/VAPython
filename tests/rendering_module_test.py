# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random

import pytest
from betterproto.lib.google import protobuf

import vapython._helper as va_helper
import vapython.vanet._helper as va_grpc_helper
import vapython.vanet._vanet_grpc as va_grpc

from .utils import random_grpc_struct, random_string, random_struct


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
@pytest.mark.parametrize(
    "filter_modules",
    [True, False],
)
async def test_get_rendering_modules(mocked_connection, mocker, test_input, filter_modules):
    va, service = mocked_connection

    method_name = "get_rendering_modules"

    info = va_grpc.AudioRendererInfo(
        name=random_string(5),
        description=random_string(5),
        enabled=random.choice([True, False]),
        output_detector_enabled=random.choice([True, False]),
        output_recording_enabled=random.choice([True, False]),
        output_recording_file_path=random_string(5),
        parameters=random_grpc_struct(),
    )
    reply = va_grpc.AudioRendererInfos(
        audio_renderer_infos=[info for _ in range(test_input)],
    )

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(only_enabled=filter_modules)

    getattr(service, method_name).assert_called_once_with(
        va_grpc.GetRenderingModulesRequest(only_enabled=filter_modules)
    )
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "muted",
    [True, False],
)
async def test_get_rendering_module_muted(mocked_connection, mocker, muted):
    va, service = mocked_connection

    method_name = "get_rendering_module_muted"
    message_name = "GetRenderingModuleMutedRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.BoolValue(muted), autospec=True)

    identifier = random_string(5)

    function = getattr(va, method_name)

    ret_val = function(identifier)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(identifier))
    assert ret_val == muted


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "muted",
    [True, False],
)
async def test_set_rendering_module_muted(mocked_connection, mocker, muted):
    va, service = mocked_connection

    method_name = "set_rendering_module_muted"
    message_name = "SetRenderingModuleMutedRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    string = random_string(5)

    function = getattr(va, method_name)

    function(string, muted=muted)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(string, muted))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random.uniform(-1, 1)) for _ in range(5)],
)
async def test_set_rendering_module_gain(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_rendering_module_gain"
    message_name = "SetRenderingModuleGainRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random.uniform(-1, 1)) for _ in range(5)],
)
async def test_get_rendering_module_gain(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_rendering_module_gain"
    message_name = "GetRenderingModuleGainRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.DoubleValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random_struct(),
        )
        for _ in range(5)
    ],
)
async def test_set_rendering_module_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_rendering_module_parameters"
    message_name = "SetRenderingModuleParametersRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc_helper.convert_struct_to_vanet(test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random_struct(),
            random_grpc_struct(),
        )
        for _ in range(5)
    ],
)
async def test_get_rendering_module_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_rendering_module_parameters"
    message_name = "GetRenderingModuleParametersRequest"

    mocker.patch.object(service, method_name, return_value=test_input[2], autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == va_grpc_helper.convert_struct_from_vanet(test_input[2])


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        ("+DS", 0),
        ("+DS,-ER", 0),
        ("+DS,-ER,-SD", 5),
        ("+DS,-ER,-SD", 8191),
    ],
)
async def test_set_rendering_module_auralization_mode(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    mocker.patch.object(service, "set_rendering_module_auralization_mode", return_value=protobuf.Empty(), autospec=True)
    mocker.patch.object(
        service,
        "get_rendering_module_auralization_mode",
        return_value=protobuf.Int32Value(value=test_input[1]),
        autospec=True,
    )

    identifier = random_string(5)

    va.set_rendering_module_auralization_mode(identifier, test_input[0])

    service.set_rendering_module_auralization_mode.assert_called_once_with(
        va_grpc.SetRenderingModuleAuralizationModeRequest(identifier, va_helper.parse_aura_mode_str(*test_input))
    )
    assert service.get_rendering_module_auralization_mode.called


@pytest.mark.asyncio
@pytest.mark.parametrize("aura_mode", [0, 1, 4096, 8191, 5, 7, 10])
async def test_get_rendering_module_auralization_mode(mocked_connection, mocker, aura_mode):
    va, service = mocked_connection

    mocker.patch.object(
        service,
        "get_rendering_module_auralization_mode",
        return_value=protobuf.Int32Value(value=aura_mode),
        autospec=True,
    )

    identifier = random_string(5)

    ret_val = va.get_rendering_module_auralization_mode(identifier)

    service.get_rendering_module_auralization_mode.assert_called_once_with(
        va_grpc.GetRenderingModuleAuralizationModeRequest(identifier)
    )
    assert ret_val == va_helper.convert_aura_mode_to_str(aura_mode)
