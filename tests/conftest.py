# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import asyncio

import pytest_asyncio
from grpclib.testing import ChannelFor

import vapython as vapy
import vapython.vanet._vanet_grpc as va_grpc


@pytest_asyncio.fixture(scope="session")
async def mocked_connection(session_mocker):
    service = va_grpc.VaBase()

    async with ChannelFor([service]) as channel:
        session_mocker.patch("vapython.vanet._va_interface.Channel.__new__", return_value=channel)
        va = vapy.VA()
        session_mocker.patch.object(va, "_get_state", autospec=True)
        va.connect(add_event_handling=False)
        yield va, service

    # return the "ownership" of the event loop back to the pytest-asyncio thread
    asyncio.set_event_loop(va._loop)
    va.disconnect()
