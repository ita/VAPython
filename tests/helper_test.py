# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

from pathlib import Path

import pytest

from vapython._helper import (
    convert_aura_mode_to_str,
    default_auralization_mode,
    find_server_executable,
    parse_aura_mode_str,
    possible_auralization_modes,
)


def test_parse_aura_mode_str():
    # Test case 1: Empty input string should return 0
    assert parse_aura_mode_str("", 0) == 0

    # Test case 2: Input string "*" should return the sum of all possible aura modes
    assert parse_aura_mode_str("*", 0) == sum(m[0] for m in possible_auralization_modes.values())

    # Test case 3: Input string "default" should return the default aura mode
    assert parse_aura_mode_str("default", 0) == default_auralization_mode

    # Test case 4: Input string "+AB,-SD" should enable AB and disable SD
    assert parse_aura_mode_str("+AB,-SD", 0) == 4096

    # Test case 5: Input string "+DS,-ER,SD" should enable DS, disable ER
    assert parse_aura_mode_str("+DS,-ER,SD", 0) == 1

    # Test case 6: Input string "-DS,-ER,SD" with aura_mode = 5 should enable DS, disable ER
    assert parse_aura_mode_str("-DS,-ER,SD", 5) == 4

    # Test case 7: Input string "-DS,+ER,SD" with aura_mode = default_aura_mode should disable DS, enable ER
    assert (
        parse_aura_mode_str("-DS,+ER,SD", default_auralization_mode)
        == default_auralization_mode - possible_auralization_modes["DS"][0] + possible_auralization_modes["ER"][0]
    )


def test_parse_aura_mode_str_invalid_input():
    # Test case 1: Invalid aura mode
    with pytest.raises(ValueError, match="Invalid aura mode: XX"):
        parse_aura_mode_str("+XX", 0)

    # Test case 2: Invalid aura mode
    with pytest.raises(ValueError, match="Invalid aura mode: XX"):
        parse_aura_mode_str("-XX", 0)


def test_convert_aura_mode_to_str():
    # Test case 1: aura_mode = 0
    assert convert_aura_mode_to_str(0, short_form=False) == "Nothing"
    assert convert_aura_mode_to_str(0, short_form=True) == ""

    # Test case 2: aura_mode = sum of all possible_aura_modes values, aura_mode = 8191 (binary: 1111111111111)
    assert convert_aura_mode_to_str(sum(m[0] for m in possible_auralization_modes.values()), short_form=False) == "All"
    assert convert_aura_mode_to_str(8191) == "DS,ER,DD,SD,MA,TV,SC,DF,NF,DP,SL,TR,AB"

    # Test case 3: aura_mode = default_aura_mode
    assert convert_aura_mode_to_str(default_auralization_mode, short_form=False) == "Default"

    # Test case 4: aura_mode = 5 (binary: 101)
    assert convert_aura_mode_to_str(5) == "DS,DD"

    # Test case 5: aura_mode = 7 (binary: 111)
    assert convert_aura_mode_to_str(7) == "DS,ER,DD"

    # Test case 6: aura_mode = 10 (binary: 1010)
    assert convert_aura_mode_to_str(10) == "ER,SD"

    # Test case 7: aura_mode = 15 (binary: 1111)
    assert convert_aura_mode_to_str(15) == "DS,ER,DD,SD"

    # Test case 8: aura_mode = 4096 (binary: 1000000000000)
    assert convert_aura_mode_to_str(4096) == "AB"

    # Test case 9: aura_mode = 1 (binary: 1)
    assert convert_aura_mode_to_str(1, short_form=False) == "Direct Sound"


def test_find_server_executable(mocker, tmp_path):
    import shutil
    import subprocess
    import tkinter as tk

    mocker.patch(
        "subprocess.run", autospec=True, return_value=subprocess.CompletedProcess("", 0, b"VACore 2023.b (release)")
    )
    mocker.patch("vapython._helper.user_config_dir", return_value=tmp_path, autospec=True)
    mocker.patch("vapython._helper.metadata.version", return_value="2023.1", autospec=True)

    config_file = tmp_path / "vapython.cfg"

    mocker.patch("shutil.which", return_value="string", autospec=True)
    result = find_server_executable()

    assert result == Path("string")

    shutil.which.assert_called_once_with("VAServer")

    assert config_file.exists()

    config_file.unlink()

    mocker.patch("shutil.which", return_value=None)
    mocker.patch("tkinter.filedialog.askopenfilename", return_value="tkstring", autospec=True)

    result = find_server_executable()

    assert result == Path("tkstring")
    shutil.which.assert_called_with("VAServer")
    tk.filedialog.askopenfilename.assert_called_once()

    assert config_file.exists()

    config_file.unlink()

    mocker.patch("tkinter.filedialog.askopenfilename", return_value="")

    result = find_server_executable()

    assert result is None
    shutil.which.assert_called_with("VAServer")
    tk.filedialog.askopenfilename.assert_called()
