# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random

import pytest
from betterproto.lib.google import protobuf

import vapython.vanet._helper as va_grpc_helper
import vapython.vanet._vanet_grpc as va_grpc

from .utils import random_grpc_struct, random_string, random_struct


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
@pytest.mark.parametrize(
    "filter_modules",
    [True, False],
)
async def test_get_reproduction_modules(mocked_connection, mocker, test_input, filter_modules):
    va, service = mocked_connection

    method_name = "get_reproduction_modules"

    info = va_grpc.AudioReproductionInfo(
        id=random_string(5),
        description=random_string(5),
        enabled=random.choice([True, False]),
        input_detector_enabled=random.choice([True, False]),
        input_recording_enabled=random.choice([True, False]),
        input_recording_file_path=random_string(5),
        bool_detector_enabled=random.choice([True, False]),
        bool_recording_enabled=random.choice([True, False]),
        bool_recording_file_path=random_string(5),
        parameters=random_grpc_struct(),
    )
    reply = va_grpc.AudioReproductionInfos(
        audio_reproduction_infos=[info for _ in range(test_input)],
    )

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(only_enabled=filter_modules)

    getattr(service, method_name).assert_called_once_with(
        va_grpc.GetReproductionModulesRequest(only_enabled=filter_modules)
    )
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "muted",
    [True, False],
)
async def test_get_reproduction_module_muted(mocked_connection, mocker, muted):
    va, service = mocked_connection

    method_name = "get_reproduction_module_muted"
    message_name = "GetReproductionModuleMutedRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.BoolValue(muted), autospec=True)

    identifier = random_string(5)

    function = getattr(va, method_name)

    ret_val = function(identifier)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(identifier))
    assert ret_val == muted


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "muted",
    [True, False],
)
async def test_set_reproduction_module_muted(mocked_connection, mocker, muted):
    va, service = mocked_connection

    method_name = "set_reproduction_module_muted"
    message_name = "SetReproductionModuleMutedRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    string = random_string(5)

    function = getattr(va, method_name)

    function(string, muted=muted)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(string, muted))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random.uniform(-1, 1)) for _ in range(5)],
)
async def test_set_reproduction_module_gain(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_reproduction_module_gain"
    message_name = "SetReproductionModuleGainRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random.uniform(-1, 1)) for _ in range(5)],
)
async def test_get_reproduction_module_gain(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_reproduction_module_gain"
    message_name = "GetReproductionModuleGainRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.DoubleValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random_struct(),
        )
        for _ in range(5)
    ],
)
async def test_set_reproduction_module_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_reproduction_module_parameters"
    message_name = "SetReproductionModuleParametersRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc_helper.convert_struct_to_vanet(test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random_struct(),
            random_grpc_struct(),
        )
        for _ in range(5)
    ],
)
async def test_get_reproduction_module_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_reproduction_module_parameters"
    message_name = "GetReproductionModuleParametersRequest"

    mocker.patch.object(service, method_name, return_value=test_input[2], autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == va_grpc_helper.convert_struct_from_vanet(test_input[2])
