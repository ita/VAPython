# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random

import pytest
from betterproto.lib.google import protobuf

import vapython.vanet._vanet_grpc as va_grpc

from .utils import random_string


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random_string(5)) for _ in range(5)],
)
async def test_set_sound_signal_source(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_sound_source_signal_source"
    message_name = "SetSoundSourceSignalSourceRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random_string(5)) for _ in range(5)],
)
async def test_get_sound_signal_source(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_sound_source_signal_source"
    message_name = "GetSoundSourceSignalSourceRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
async def test_remove_sound_source_signal_source(mocked_connection, mocker, test_input):  # noqa: ARG001
    va, service = mocked_connection

    public_method_name = "remove_sound_source_signal_source"
    method_name = "set_sound_source_signal_source"
    message_name = "SetSoundSourceSignalSourceRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, public_method_name)

    source_id = random.randint(0, 100)

    function(source_id)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(source_id, ""))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.uniform(-1, 1)) for _ in range(5)],
)
async def test_set_sound_sound_power(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_sound_source_sound_power"
    message_name = "SetSoundSourceSoundPowerRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.uniform(-1, 1)) for _ in range(5)],
)
async def test_get_sound_sound_power(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_sound_source_sound_power"
    message_name = "GetSoundSourceSoundPowerRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.DoubleValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]
