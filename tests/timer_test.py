# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random
import time

import pytest


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", range(5))
async def test_close_timer(mocked_connection, test_input):  # noqa: ARG001
    va, _ = mocked_connection

    va._timer_interval = random.uniform(0, 10)

    va.close_timer()

    assert va._timer_interval == -1


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", range(5))
async def test_set_timer(mocked_connection, test_input):  # noqa: ARG001
    va, _ = mocked_connection

    interval = random.uniform(0, 10)

    va.set_timer(interval)

    assert va._timer_interval == interval


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", range(5))
async def test_set_timer_wrong_input(mocked_connection, test_input):  # noqa: ARG001
    va, _ = mocked_connection

    interval = random.uniform(-10, -1)

    with pytest.raises(ValueError, match="Interval must be greater"):
        va.set_timer(interval)


@pytest.mark.asyncio
@pytest.mark.parametrize("test_input", range(5))
@pytest.mark.xfail(reason="This test is not reliable")
async def test_wait_for_timer(mocked_connection, test_input):  # noqa: ARG001
    va, _ = mocked_connection

    interval = random.uniform(1e-3, 10e-3)
    interval_ns = interval * 1e9
    dummy_execution_time_ratio = random.uniform(0, 0.8)

    va.set_timer(interval)

    runs = 100
    diffs = []
    start = time.perf_counter_ns()
    for _ in range(runs):
        time.sleep(interval * dummy_execution_time_ratio)  # noqa: ASYNC101

        va.wait_for_timer()

        now = time.perf_counter_ns()
        diffs.append(now - start)
        start = now

    # remove first 5 values as they may not accurate
    diffs = diffs[5:]

    avg = sum(diffs) / len(diffs)
    assert avg >= interval_ns
    assert pytest.approx(interval_ns, abs=1e6) == avg

    for i, diff in enumerate(diffs):
        assert pytest.approx(interval_ns, abs=1e6) == diff, f"Failed at index {i} with diff {diff}"
