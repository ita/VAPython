# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random
from pathlib import Path

import pytest
from betterproto.lib.google import protobuf

import vapython.vanet._helper as va_grpc_helper
import vapython.vanet._vanet_grpc as va_grpc

from .utils import random_grpc_struct, random_string, random_struct


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random_struct(), random_string(5)) for _ in range(5)],
)
async def test_create_signal_source_prototype_from_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "create_signal_source_prototype_from_parameters"
    message_name = "CreateSignalSourcePrototypeFromParametersRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[2]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(name=test_input[0], parameters=test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == test_input[2]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random_struct(), random_string(5)) for _ in range(5)],
)
async def test_create_signal_source_buffer_from_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "create_signal_source_buffer_from_parameters"
    message_name = "CreateSignalSourceBufferFromParametersRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[2]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(name=test_input[0], parameters=test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            name=test_input[0], parameters=va_grpc_helper.convert_struct_to_vanet(test_input[1])
        )
    )
    assert ret_val == test_input[2]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
@pytest.mark.parametrize(
    "path_type",
    [str, Path],
)
async def test_create_signal_source_buffer_from_file(mocked_connection, mocker, test_input, path_type):  # noqa: ARG001
    va, service = mocked_connection

    public_method_name = "create_signal_source_buffer_from_file"
    method_name = "create_signal_source_buffer_from_parameters"
    message_name = "CreateSignalSourceBufferFromParametersRequest"

    identifier = random_string(5)

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(identifier), autospec=True)

    test_path = path_type(random_string(5))

    function = getattr(va, public_method_name)

    ret_val = function(file_path=test_path)

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            name="", parameters=va_grpc_helper.convert_struct_to_vanet({"filepath": str(test_path)})
        )
    )
    assert ret_val == identifier


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random_string(5)) for _ in range(5)],
)
async def test_create_signal_source_text_to_speech(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "create_signal_source_text_to_speech"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(va_grpc.CreateSignalSourceTextToSpeechRequest(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random_string(5)) for _ in range(5)],
)
async def test_create_signal_source_sequencer(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "create_signal_source_sequencer"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(va_grpc.CreateSignalSourceSequencerRequest(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random_struct(), random_string(5)) for _ in range(5)],
)
async def test_create_signal_source_engine(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "create_signal_source_engine"
    message_name = "CreateSignalSourceEngineRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[2]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(name=test_input[0], parameters=test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == test_input[2]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random_struct(), random_string(5)) for _ in range(5)],
)
async def test_create_signal_source_machine(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "create_signal_source_machine"
    message_name = "CreateSignalSourceMachineRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[2]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(name=test_input[0], parameters=test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == test_input[2]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random.choice([True, False])) for _ in range(5)],
)
async def test_delete_acoustic_material(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "delete_signal_source"
    message_name = "DeleteSignalSourceRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.BoolValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
async def test_get_signal_source_info(mocked_connection, mocker, test_input):  # noqa: ARG001
    va, service = mocked_connection

    method_name = "get_signal_source_info"
    message_name = "GetSignalSourceInfoRequest"
    reply_name = "SignalSourceInfo"

    reply = getattr(va_grpc, reply_name)(
        id=random_string(5),
        name=random_string(5),
        description=random_string(5),
    )

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(reply.id)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(reply.id))
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
async def test_get_signal_source_infos(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_signal_source_infos"

    info = va_grpc.SignalSourceInfo(
        id=random_string(5),
        name=random_string(5),
        description=random_string(5),
    )
    reply = va_grpc.SignalSourceInfos(
        signal_source_infos=[info for _ in range(test_input)],
    )

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function()

    assert getattr(service, method_name).called
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
@pytest.mark.parametrize(
    "looping",
    [True, False],
)
async def test_set_signal_source_buffer_looping(mocked_connection, mocker, test_input, looping):  # noqa: ARG001
    va, service = mocked_connection

    method_name = "set_signal_source_buffer_looping"
    message_name = "SetSignalSourceBufferLoopingRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    string = random_string(5)

    function = getattr(va, method_name)

    function(string, looping=looping)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(string, looping))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
@pytest.mark.parametrize(
    "looping",
    [True, False],
)
async def test_get_signal_source_buffer_looping(mocked_connection, mocker, test_input, looping):  # noqa: ARG001
    va, service = mocked_connection

    method_name = "get_signal_source_buffer_looping"
    message_name = "GetSignalSourceBufferLoopingRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.BoolValue(looping), autospec=True)

    string = random_string(5)

    function = getattr(va, method_name)

    ret_val = function(string)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(string))
    assert ret_val == looping


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random_struct(),
        )
        for _ in range(5)
    ],
)
async def test_set_signal_source_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_signal_source_parameters"
    message_name = "SetSignalSourceParametersRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc_helper.convert_struct_to_vanet(test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random_struct(),
            random_grpc_struct(),
        )
        for _ in range(5)
    ],
)
async def test_get_signal_source_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_signal_source_parameters"
    message_name = "GetSignalSourceParametersRequest"

    mocker.patch.object(service, method_name, return_value=test_input[2], autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == va_grpc_helper.convert_struct_from_vanet(test_input[2])


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random_string(5),
            random.uniform(0, 10),
        )
        for _ in range(5)
    ],
)
async def test_set_signal_source_buffer_playback_position(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_signal_source_buffer_playback_position"
    message_name = "SetSignalSourceBufferPlaybackPositionRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            test_input[1],
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "state",
    [
        va_grpc.PlaybackStateState.INVALID,
        va_grpc.PlaybackStateState.STOPPED,
        va_grpc.PlaybackStateState.PAUSED,
        va_grpc.PlaybackStateState.PLAYING,
    ],
)
async def test_get_signal_source_buffer_playback_state(mocked_connection, mocker, state):
    va, service = mocked_connection

    method_name = "get_signal_source_buffer_playback_state"
    message_name = "GetSignalSourceBufferPlaybackStateRequest"

    mocker.patch.object(service, method_name, return_value=va_grpc.PlaybackState(state=state), autospec=True)

    identifier = random_string(5)

    function = getattr(va, method_name)

    ret_val = function(identifier)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(identifier))
    assert ret_val == state
    assert ret_val.__str__() == state.name


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        ("none", va_grpc.PlaybackActionAction.NONE),
        ("play", va_grpc.PlaybackActionAction.PLAY),
        ("pause", va_grpc.PlaybackActionAction.PAUSE),
        ("stop", va_grpc.PlaybackActionAction.STOP),
        (va_grpc.PlaybackActionAction.NONE, va_grpc.PlaybackActionAction.NONE),
        (va_grpc.PlaybackActionAction.PLAY, va_grpc.PlaybackActionAction.PLAY),
        (va_grpc.PlaybackActionAction.PAUSE, va_grpc.PlaybackActionAction.PAUSE),
        (va_grpc.PlaybackActionAction.STOP, va_grpc.PlaybackActionAction.STOP),
    ],
)
async def test_set_signal_source_buffer_playback_action(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_signal_source_buffer_playback_action"
    message_name = "SetSignalSourceBufferPlaybackActionRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    identifier = random_string(5)

    function = getattr(va, method_name)

    function(identifier, test_input[0])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            identifier,
            va_grpc.PlaybackAction(test_input[1]),
        )
    )
