# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random

import pytest
from betterproto.lib.google import protobuf

import vapython.vanet._helper as va_grpc_helper
import vapython.vanet._vanet_grpc as va_grpc
from vapython._helper import convert_aura_mode_to_str
from vapython._types import VAQuaternion, VAVector

from .utils import random_grpc_struct, random_string, random_struct


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random.randint(0, 100)) for _ in range(5)],
)
async def test_create_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"create_sound_{entity}"
    message_name = f"CreateSound{entity.capitalize()}Request"

    mocker.patch.object(service, method_name, return_value=protobuf.Int32Value(value=test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random_string(5), random.randint(0, 100)) for _ in range(5)],
)
async def test_create_explicit_renderer_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"create_sound_{entity}_explicit_renderer"
    message_name = f"CreateSound{entity.capitalize()}ExplicitRendererRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Int32Value(value=test_input[2]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))
    assert ret_val == test_input[2]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.choice([True, False])) for _ in range(5)],
)
async def test_delete_sound_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"delete_sound_{entity}"
    message_name = f"DeleteSound{entity.capitalize()}Request"

    mocker.patch.object(service, method_name, return_value=protobuf.BoolValue(value=test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.choice([True, False])) for _ in range(5)],
)
async def test_set_sound_enabled_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_enabled"
    message_name = f"SetSound{entity.capitalize()}EnabledRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], enabled=test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.choice([True, False])) for _ in range(5)],
)
async def test_get_sound_enabled_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_enabled"
    message_name = f"GetSound{entity.capitalize()}EnabledRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.BoolValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random_string(5)) for _ in range(5)],
)
async def test_set_sound_name_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_name"
    message_name = f"SetSound{entity.capitalize()}NameRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random_string(5)) for _ in range(5)],
)
async def test_get_sound_name_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_name"
    message_name = f"GetSound{entity.capitalize()}NameRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.randint(0, 100)) for _ in range(5)],
)
async def test_set_sound_directivity_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_directivity"
    message_name = f"SetSound{entity.capitalize()}DirectivityRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.randint(0, 100)) for _ in range(5)],
)
async def test_get_sound_directivity_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_directivity"
    message_name = f"GetSound{entity.capitalize()}DirectivityRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Int32Value(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.choice([True, False])) for _ in range(5)],
)
async def test_set_sound_muted_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_muted"
    message_name = f"SetSound{entity.capitalize()}MutedRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], muted=test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.choice([True, False])) for _ in range(5)],
)
async def test_get_sound_muted_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_muted"
    message_name = f"GetSound{entity.capitalize()}MutedRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.BoolValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_set_sound_pose_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_pose"
    message_name = f"SetSound{entity.capitalize()}PoseRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1], test_input[2])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc.Vector3(*test_input[1]),
            va_grpc.Quaternion(*test_input[2]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_get_sound_pose_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_pose"
    message_name = f"GetSound{entity.capitalize()}PoseRequest"
    reply_name = f"GetSound{entity.capitalize()}PoseReply"

    reply = getattr(va_grpc, reply_name)(va_grpc.Vector3(*test_input[1]), va_grpc.Quaternion(*test_input[2]))

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
        )
        for _ in range(5)
    ],
)
async def test_set_sound_position_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_position"
    message_name = f"SetSound{entity.capitalize()}PositionRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc.Vector3(*test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
        )
        for _ in range(5)
    ],
)
async def test_get_sound_position_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_position"
    message_name = f"GetSound{entity.capitalize()}PositionRequest"

    reply = va_grpc.Vector3(*test_input[1])

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == VAVector(*test_input[1])


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_set_sound_orientation_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_orientation"
    message_name = f"SetSound{entity.capitalize()}OrientationRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc.Quaternion(*test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_get_sound_orientation_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_orientation"
    message_name = f"GetSound{entity.capitalize()}OrientationRequest"

    reply = va_grpc.Quaternion(*test_input[1])

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == VAQuaternion(*test_input[1])


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "version",
    [
        "_vu",
        "_view_up",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(3)],
        )
        for _ in range(5)
    ],
)
async def test_set_sound_orientation_vu_(mocked_connection, mocker, entity, version, test_input):
    va, service = mocked_connection

    public_method_name = f"set_sound_{entity}_orientation{version}"
    method_name = f"set_sound_{entity}_orientation_vu"
    message_name = f"SetSound{entity.capitalize()}OrientationVuRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, public_method_name)

    function(test_input[0], test_input[1], test_input[2])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc.Vector3(*test_input[1]),
            va_grpc.Vector3(*test_input[2]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "version",
    [
        "_vu",
        "_view_up",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(3)],
        )
        for _ in range(5)
    ],
)
async def test_get_sound_orientation_vu_(mocked_connection, mocker, entity, version, test_input):
    va, service = mocked_connection

    public_method_name = f"get_sound_{entity}_orientation{version}"
    method_name = f"get_sound_{entity}_orientation_vu"
    message_name = f"GetSound{entity.capitalize()}OrientationVuRequest"
    reply_name = f"GetSound{entity.capitalize()}OrientationVuReply"

    reply = getattr(va_grpc, reply_name)(va_grpc.Vector3(*test_input[1]), va_grpc.Vector3(*test_input[2]))

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, public_method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            random_struct(),
        )
        for _ in range(5)
    ],
)
async def test_set_sound_parameters_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_parameters"
    message_name = f"SetSound{entity.capitalize()}ParametersRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc_helper.convert_struct_to_vanet(test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            random_struct(),
            random_grpc_struct(),
        )
        for _ in range(5)
    ],
)
async def test_get_sound_parameters_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_parameters"
    message_name = f"GetSound{entity.capitalize()}ParametersRequest"

    mocker.patch.object(service, method_name, return_value=test_input[2], autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == va_grpc_helper.convert_struct_from_vanet(test_input[2])


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100),) for _ in range(2)],
)
@pytest.mark.parametrize(
    "aura_mode",
    [
        ("+DS", 0, 1),
        ("+DS,-ER", 0, 1),
        ("+DS,-ER,-SD", 5, 5),
        ("+DS,-ER,-SD", 8191, 8181),
    ],
)
async def test_set_sound_auralization_mode_(mocked_connection, mocker, entity, test_input, aura_mode):
    va, service = mocked_connection

    method_name = f"set_sound_{entity}_auralization_mode"
    method_name_getter = f"get_sound_{entity}_auralization_mode"
    message_name = f"SetSound{entity.capitalize()}AuralizationModeRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)
    mocker.patch.object(
        service, method_name_getter, return_value=protobuf.Int32Value(value=aura_mode[1]), autospec=True
    )

    function = getattr(va, method_name)

    function(test_input[0], aura_mode[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], aura_mode[2]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100),) for _ in range(2)],
)
@pytest.mark.parametrize(
    "aura_mode",
    [0, 1, 4096, 8191, 5, 7, 10],
)
async def test_get_sound_auralization_mode_(mocked_connection, mocker, entity, test_input, aura_mode):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_auralization_mode"
    message_name = f"GetSound{entity.capitalize()}AuralizationModeRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Int32Value(aura_mode), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == convert_aura_mode_to_str(aura_mode)


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [([random.randint(0, 100) for _ in range(3)],) for _ in range(5)],
)
async def test_get_sound_ids_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_ids"
    method_name_server = f"get_sound_{entity}_i_ds"

    mocker.patch.object(service, method_name_server, return_value=va_grpc.IntIdVector(test_input[0]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function()

    assert getattr(service, method_name_server).called
    assert ret_val == va_grpc.IntIdVector(test_input[0])


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "entity",
    [
        "source",
        "receiver",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100),) for _ in range(5)],
)
async def test_get_sound_info_(mocked_connection, mocker, entity, test_input):
    va, service = mocked_connection

    method_name = f"get_sound_{entity}_info"
    reply_name = f"Sound{entity.capitalize()}Info"

    reply = getattr(va_grpc, reply_name)(
        id=test_input[0],
        name=random_string(5),
        explicit_renderer_id=random_string(5),
    )

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    assert getattr(service, method_name).called
    assert ret_val == reply
