# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random

import pytest
from betterproto.lib.google import protobuf

import vapython.vanet._vanet_grpc as va_grpc
from vapython._types import VAQuaternion


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_set_sound_receiver_head_above_torso_orientation(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_sound_receiver_head_above_torso_orientation"
    message_name = "SetSoundReceiverHeadAboveTorsoOrientationRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc.Quaternion(*test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_get_sound_receiver_head_above_torso_orientation(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_sound_receiver_head_above_torso_orientation"
    message_name = "GetSoundReceiverHeadAboveTorsoOrientationRequest"

    reply = va_grpc.Quaternion(*test_input[1])

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == VAQuaternion(*test_input[1])


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "public_method_name",
    [
        "set_sound_receiver_real_world_position_orientation_vu",
        "set_sound_receiver_real_world_position_orientation_view_up",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(3)],
        )
        for _ in range(5)
    ],
)
async def test_set_sound_receiver_real_world_position_orientation_vu(
    mocked_connection, mocker, public_method_name, test_input
):
    va, service = mocked_connection

    method_name = "set_sound_receiver_real_world_position_orientation_vu"
    message_name = "SetSoundReceiverRealWorldPositionOrientationVuRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, public_method_name)

    function(test_input[0], test_input[1], test_input[2], test_input[3])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc.Vector3(*test_input[1]),
            va_grpc.Vector3(*test_input[2]),
            va_grpc.Vector3(*test_input[3]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "public_method_name",
    [
        "get_sound_receiver_real_world_position_orientation_vu",
        "get_sound_receiver_real_world_head_position_orientation_view_up",
    ],
)
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(3)],
        )
        for _ in range(5)
    ],
)
async def test_get_sound_receiver_real_world_position_orientation_vu(
    mocked_connection, mocker, public_method_name, test_input
):
    va, service = mocked_connection

    method_name = "get_sound_receiver_real_world_position_orientation_vu"
    message_name = "GetSoundReceiverRealWorldPositionOrientationVuRequest"
    reply_name = "GetSoundReceiverRealWorldPositionOrientationVuReply"

    reply = getattr(va_grpc, reply_name)(
        va_grpc.Vector3(*test_input[1]), va_grpc.Vector3(*test_input[2]), va_grpc.Vector3(*test_input[3])
    )

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, public_method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_set_sound_receiver_real_world_pose(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_sound_receiver_real_world_pose"
    message_name = "SetSoundReceiverRealWorldPoseRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1], test_input[2])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc.Vector3(*test_input[1]),
            va_grpc.Quaternion(*test_input[2]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(3)],
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_get_sound_receiver_real_world_pose(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_sound_receiver_real_world_pose"
    message_name = "GetSoundReceiverRealWorldPoseRequest"
    reply_name = "GetSoundReceiverRealWorldPoseReply"

    reply = getattr(va_grpc, reply_name)(va_grpc.Vector3(*test_input[1]), va_grpc.Quaternion(*test_input[2]))

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_set_sound_receiver_real_world_head_above_torso_orientation(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_sound_receiver_real_world_head_above_torso_orientation"
    message_name = "SetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc.Quaternion(*test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            [random.uniform(-100, 100) for _ in range(4)],
        )
        for _ in range(5)
    ],
)
async def test_get_sound_receiver_real_world_head_above_torso_orientation(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_sound_receiver_real_world_head_above_torso_orientation"
    message_name = "GetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest"

    reply = va_grpc.Quaternion(*test_input[1])

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == VAQuaternion(*test_input[1])
