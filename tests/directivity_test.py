# SPDX-FileCopyrightText: 2024-present Pascal Palenda <pascal.palenda@akustik.rwth-aachen.de>
#
# SPDX-License-Identifier: Apache-2.0

import random
from pathlib import Path

import pytest
from betterproto.lib.google import protobuf

import vapython.vanet._helper as va_grpc_helper
import vapython.vanet._vanet_grpc as va_grpc

from .utils import random_grpc_struct, random_string, random_struct


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random_string(5), random_struct(), random.randint(0, 100)) for _ in range(5)],
)
async def test_create_directivity_from_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "create_directivity_from_parameters"
    message_name = "CreateDirectivityFromParametersRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Int32Value(test_input[2]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(name=test_input[0], parameters=test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == test_input[2]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
@pytest.mark.parametrize(
    "path_type",
    [str, Path],
)
async def test_create_directivity_from_file(mocked_connection, mocker, test_input, path_type):  # noqa: ARG001
    va, service = mocked_connection

    public_method_name = "create_directivity_from_file"
    method_name = "create_directivity_from_parameters"
    message_name = "CreateDirectivityFromParametersRequest"

    identifier = random.randint(0, 100)

    mocker.patch.object(service, method_name, return_value=protobuf.Int32Value(identifier), autospec=True)

    test_path = path_type(random_string(5))

    function = getattr(va, public_method_name)

    ret_val = function(test_path)

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)("", va_grpc_helper.convert_struct_to_vanet({"filepath": str(test_path)}))
    )
    assert ret_val == identifier


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random.choice([True, False])) for _ in range(5)],
)
async def test_delete_directivity(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "delete_directivity"
    message_name = "DeleteDirectivityRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.BoolValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
async def test_get_directivity_info(mocked_connection, mocker, test_input):  # noqa: ARG001
    va, service = mocked_connection

    method_name = "get_directivity_info"
    message_name = "GetDirectivityInfoRequest"
    reply_name = "DirectivityInfo"

    reply = getattr(va_grpc, reply_name)(
        id=random.randint(0, 100),
        name=random_string(5),
        parameters=random_grpc_struct(),
    )

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function(reply.id)

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(reply.id))
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    range(5),
)
async def test_get_directivity_infos(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_directivity_infos"

    info = va_grpc.DirectivityInfo(
        id=random.randint(0, 100),
        name=random_string(5),
        parameters=random_grpc_struct(),
    )
    reply = va_grpc.DirectivityInfosReply(
        directivity_infos=[info for _ in range(test_input)],
    )

    mocker.patch.object(service, method_name, return_value=reply, autospec=True)

    function = getattr(va, method_name)

    ret_val = function()

    assert getattr(service, method_name).called
    assert ret_val == reply


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random_string(5)) for _ in range(5)],
)
async def test_set_directivity_name(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_directivity_name"
    message_name = "SetDirectivityNameRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0], test_input[1]))


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [(random.randint(0, 100), random_string(5)) for _ in range(5)],
)
async def test_get_directivity_name(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_directivity_name"
    message_name = "GetDirectivityNameRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.StringValue(test_input[1]), autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0])

    getattr(service, method_name).assert_called_once_with(getattr(va_grpc, message_name)(test_input[0]))
    assert ret_val == test_input[1]


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            random_struct(),
        )
        for _ in range(5)
    ],
)
async def test_set_directivity_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "set_directivity_parameters"
    message_name = "SetDirectivityParametersRequest"

    mocker.patch.object(service, method_name, return_value=protobuf.Empty(), autospec=True)

    function = getattr(va, method_name)

    function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(
            test_input[0],
            va_grpc_helper.convert_struct_to_vanet(test_input[1]),
        )
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "test_input",
    [
        (
            random.randint(0, 100),
            random_struct(),
            random_grpc_struct(),
        )
        for _ in range(5)
    ],
)
async def test_get_directivity_parameters(mocked_connection, mocker, test_input):
    va, service = mocked_connection

    method_name = "get_directivity_parameters"
    message_name = "GetDirectivityParametersRequest"

    mocker.patch.object(service, method_name, return_value=test_input[2], autospec=True)

    function = getattr(va, method_name)

    ret_val = function(test_input[0], test_input[1])

    getattr(service, method_name).assert_called_once_with(
        getattr(va_grpc, message_name)(test_input[0], va_grpc_helper.convert_struct_to_vanet(test_input[1]))
    )
    assert ret_val == va_grpc_helper.convert_struct_from_vanet(test_input[2])
