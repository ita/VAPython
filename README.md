# VAPython

VAPython is a pure Python binding for controlling a [Virtual Acoustics (VA)](https://www.virtualacoustics.org/) server.

It utilizes the gRPC protocol to communicate with the VA server and provides a Pythonic interface to the VA API.
This is achieved through the use of [betterproto](https://github.com/danielgtaylor/python-betterproto).

## Requirements

Python 3.9 or higher.

## Install

The downloaded package contains a wheel file that can be installed using pip:

```bash
pip install VAPython-<version>-py3-none-any.whl
```

where `<version>` is the version of the package,e.g. `v2024a`.

Once installed, the package can be imported and used like this:

```python
from vapython import VA

va = VA()    # create a VA object
va.connect() # connect to the VA server
```

This way, the Python commands will be comparable to the commands of VAMatlab.

## Examples

This binding includes examples that demonstrate how to use the API.
These can be found in the `examples` directory of the package.

When VAPython is installed, a list of examples can be shown using the example runner:
```bash
python -m vapython.examples
```
By entering the respective number, one of the examples can be selected and opened.
Note, that this will use the default application for `.py` files that is registered with your operating system.
This might not be a text editor.

Alternatively, a specific example can directly be executed using the respective name:
```bash
python -m vapython.examples.<example_name>
```

## Development guide

VAPython uses [hatch](https://hatch.pypa.io/dev/) for packaging and building.

Building in this case means generating the Python files from the proto files and rendering the template for the wrapper.
In case changes are made to the proto files, the build scripts or the template, the build process has to be triggered manually.
This can be done by running the following command in the root directory of the project:

```bash
hatch build
```

Optionally,

```bash
hatch build -t wheel
```

can be used to only build a wheel package (and save some time).

When only changes are made to the Python files, the build process is not necessary.
In this case it is recommended to use the default environment of hatch which includes a editable install of the package.
This is also the environment in which the tests are run.

### Running the examples

When developing and using hatch, the examples runner can also be called via hatch:

```bash
hatch run examples
```

## License

Copyright 2015-2024 Institute of Technical Acoustics (ITA), RWTH Aachen University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use files of this project except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
