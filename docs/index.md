# VAPython

![doccoverage](docstr_coverage_badge.svg)

This is the API documentation for the VAPython package.
VAPython is a Python package that provides a Pythonic interface to [Virtual Acoustics (VA)](https://www.virtualacoustics.org/VA/).
VA is a real-time auralization framework for scientific research providing modules and interfaces for experiments and demonstrations. It is open source and fully controllable enabling reproducible research.

## Prototyping

You will find, that certain functionality seems to be implemented in multiple methods with different input parameters.
This is because VA interfaces make use of prototyping methods, which usually have names similar to
`va.set_..._parameters()` or `va.get_..._parameters()`. More information is given
[here](https://www.virtualacoustics.org/VA/documentation/control/#prototyping).


::: vapython.va.VA

::: vapython._helper.possible_auralization_modes
    handler: python
    options:
        show_if_no_docstring: true
        line_length: 60

::: vapython._helper.default_auralization_mode
    handler: python
    options:
        show_if_no_docstring: true
        line_length: 60
